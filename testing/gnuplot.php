<?php

/**
 * A test for the PHP_GNUPlot script by Liu Yi.
 * 
 * @author Matthias Bach <marix@marix.org>
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

// get gnuplot script
require_once('../www/contrib/PHP_GNUPlot.php');
require_once("../www/contrib/timer.php");

$scriptTimer = new Timer();

// how to reference gnuplot
$GNUPLOT = "gnuplot";

// how often to do for statistics
$RUNS = 33;

$smallPlotTime = 0;
$smallAllTime = 0;
$largePlotTime = 0;
$largeAllTime = 0;

for( $run = 0; $run < $RUNS; $run++ ) {

	$smallAllTimer = new Timer();
	
	$data = new PGData('Test Data');
	$data->addDataEntry( array(1, 2) );
	$data->addDataEntry( array(2, 4) );
	$data->addDataEntry( array(3, 4) );
	$data->addDataEntry( array(4, 1) );
	
	$data3d = new PGData('3D Data');
	$data3d->addDataEntry( array(0, 0, 1 ) );
	$data3d->addDataEntry( array(0, 1, 2 ) );
	$data3d->addDataEntry( array(1, 0, 2 ) );
	$data3d->addDataEntry( array(2, 2, 2 ) );
	$data3d->addDataEntry( array(3, 1, 3 ) );
	
	$smallPlotTimer = new Timer();
	
	$p = new GNUPlot();
	
	$p->setTitle("PHP Gnuplot Test");
	
	$p->setSize( .6, .6 );
	
	$p->setRange("y", 0, 5);
	$p->setRange("x", 0, 5);
	
	$p->setOutput( "gnuplot2d.png" );
	
	$p->plotData( $data, 'histeps', '1:2' );
	
	
	$p->setRange("z", 0, 5 );
	
	$p->setOutput( "gnuplot3d.png" );
	
	$p->splotData( $data3d, 'impulses', '1:2:3', 'lw 30' );
	
	$p->close();
	
	$smallPlotTimer->stop();
	$smallAllTimer->stop();
	
	$largeAllTimer = new Timer();
	
	// if you always want the same data, srand to fixed value
	// srand(102312);
	$data = new PGData('Random data');
	
	for( $i = 0; $i < 1024; $i++ ) 
		$data->addDataEntry( array( $i, rand( 0, 100 ) ) );
		
	$largePlotTimer = new Timer();
	
	$p = new GNUPlot();
	
	$p->setTitle("PHP Gnuplot Test");
	
	$p->setSize( 2, .4 );
	
	$p->setRange("y", 0, 100);
	$p->setRange("x", 0, 1024);
	
	$p->setOutput( "gnuplotLargeData.png" );
	
	$p->plotData( $data, 'boxes', '1:2' );
	
	$p->close();
	
	
	$largePlotTimer->stop();
	$largeAllTimer->stop();
	
	$smallPlotTime += $smallPlotTimer->getTime();
	$smallAllTime += $smallAllTimer->getTime();
	
	$largePlotTime += $largePlotTimer->getTime();
	$largeAllTime += $largeAllTimer->getTime();
}

$scriptTimer->stop();

echo "Two Small Datasets\n";
echo "Pure Plotting :        " .  round( ( $smallPlotTime / (float) $RUNS ), 2) . " s.\n";
echo "Plotting and filling : " .  round( ( $smallAllTime / (float) $RUNS ), 2) . " s.\n";
echo "\n";
echo "One large Dataset\n";
echo "Pure Plotting :        " . round( ( $largePlotTime / (float) $RUNS ), 2) . " s.\n";
echo "Plotting and filling : " . round( ( $largeAllTime / (float) $RUNS ), 2) . " s.\n";
echo "\n";
echo "Script execution     : " . $scriptTimer->getTime() . " s.\n";

?>
