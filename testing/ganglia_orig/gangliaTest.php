<?php
/**
 * Just measuring how long original ganglia needs for parsing
 */

require_once("../../contrib/timer.php");
require_once("ganglia.php");

$scriptTimer = new Timer();

// define vars used by ganglia

$context = "cluster";
$ganglia_ip = "localhost";
$ganglia_port = "8649";

$parseTimer = new Timer();

if( !Gmetad() ) {
	echo "Ganglia parsing failed: " . $error . "\n";
};

$parseTimer->stop();

$scriptTimer->stop();
  
echo "Script execution : " . $scriptTimer->getTime() . " s\n";
echo "Parsing :          " . $parseTimer->getTime() . " s\n";
echo "Number of hosts :  " . ( $cluster['HOSTS_UP'] + $cluster['HOSTS_DOWN'] ) . "\n";


?>