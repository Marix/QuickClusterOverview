#!/usr/bin/php
<?php

/**
 * Just a litle ganglia test utilty
 * 
 * This provides an endpoint a ganglia client can connect to for
 * recieving a ganglia xml. It puts out the content of a ganglia
 * xml file and closes the connection just as ganglia would.
 *
 * This watis for NUM_CONNECTS connections before it closes down.
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

error_reporting(E_ALL);

// Allow the script to hang around waiting for connections
set_time_limit(0);

// Where to bind this to
$ADDRESS = '127.0.0.1';
$PORT = 8649;

$NUM_CONNECTS = 10;

// file to return
$GANGLIA_FILE = "data/ganglia2.dat";
// how large chunks to read
$CHUNK_SIZE = 4 * 1024;

if (($sock = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) < 0) {
   echo "socket_create() failed: " . socket_strerror($sock) . "\n";
   die;
}

// bind socket to address
if (($ret = socket_bind($sock, $ADDRESS, $PORT)) < 0) {
   echo "socket_bind() failed: " . socket_strerror($ret) . "\n";
   socket_close( $sock );
   die;
}

// listen for connection
if (($ret = socket_listen($sock, 5)) < 0) {
   echo "socket_listen() failed: " . socket_strerror($ret) . "\n";
   socket_close( $sock );
   die;
}

echo "Listening on $PORT.\n";

for ( $iConn = 0; $iConn < $NUM_CONNECTS; $iConn++ ) {
	// hang around for connection ( socket_accept blocks )
	if (($msgsock = socket_accept($sock)) < 0) {
		echo "socket_accept() failed: " . socket_strerror($msgsock) . "\n";
		socket_close( $sock );
		die;
	}
	
	$fp = fopen( $GANGLIA_FILE, "r" );
	if( !$fp ) {
		socket_close( $msgsock );
		socket_close( $sock );
		die( "Failed to open ganglia file for reading.\n" );
	}
	
	// write file content to socket
	while( $datachunk = fread( $fp, $CHUNK_SIZE ) ) {
		// this is  somewhat unclean as we don't check how much ist actually written
		socket_write( $msgsock, $datachunk, strlen( $datachunk ) );
	}
	
	fclose( $fp );
	
	echo "Closing socket \$msgsock\n";
	socket_close( $msgsock );
}

echo "Closing socket \$sock\n";
socket_close( $sock );

?>
