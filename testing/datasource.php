#!/usr/bin/php
<?php
/**
 * A generic datasource test
 *
 * This can test generic datasource pluging by calling
 * the the same as they would be called in the real code,
 * but only measuring execution time and optionally
 * printing the results the datasource returned.
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
// start timing
require_once("../www/contrib/timer.php");
$scriptTimer = new Timer();

// parse command line switches
if( $_SERVER['argc'] < 4 ) {
	die( "Usage: datasource.php plugin host port [metric ...]\n" );
}
$PLUGIN_NAME = $_SERVER['argv'][1];
$HOST = $_SERVER['argv'][2];
$PORT = $_SERVER['argv'][3];

// show result
$OUTPUT = TRUE;


// define data structures
$PROVIDER = array( array( 'name' => $HOST, 'port' => $PORT ) );
$DATASOURCE = array( 'name' => $PLUGIN_NAME, 'PROVIDER' => &$PROVIDER );
$CLUSTER_DATA = array( 'DATA_SETS' => array( 0 => array( ) ) );
$DATA = array( 'CONFIG' => array('DATASOURCE' => &$DATASOURCE), 'CLUSTER_DATA' => &$CLUSTER_DATA );
 
// parse metric constraints if given
for( $iLoop = 4; $iLoop < $_SERVER['argc']; $iLoop++ ) {
	$metricName = $_SERVER['argv'][$iLoop];
	$CLUSTER_DATA[ 'DATA_SETS' ][0][$metricName]['id'] = $metricName;
}

$pluginTimer = new Timer();

require_once( '../www/datasources/' . $PLUGIN_NAME . '.php' );

$getDataMethod = $PLUGIN_NAME . "_getData";
$getNameMethod = $PLUGIN_NAME . "_getName";
$getErrorMethod = $PLUGIN_NAME . "_getError";

$getDataTimer = new Timer();
$res = $getDataMethod( $CLUSTER_DATA[ 'DATA_SETS' ][0], $DATA['CONFIG'] );
$getDataTimer->stop();

if( $res !== TRUE ) { // check if retrieving data was successfull, else query and print error
	echo 'Retrieving data from ' . $getNameMethod() . ' failed: ' . $getErrorMethod() . "\n";
	$OUTPUT = FALSE;
}

$pluginTimer->stop();

if( $OUTPUT ) {
	foreach( $CLUSTER_DATA[ 'DATA_SETS' ][0] as $metric ) {
		echo $metric['id']; echo ":\n";
		if( is_array( $metric['NODES'] ) ) {
			foreach( $metric['NODES'] as $host ) {
				echo $host['name'] . ": \t" . $host['value'] . "\n";
			}
		}
		echo "\n";
	}
}

$scriptTimer->stop();

echo "Script execution : " . $scriptTimer->getTime() . " s\n";
echo "Plugin :           " . $pluginTimer->getTime() . " s\n";
echo "Data retrieval :   " . $getDataTimer->getTime() . " s\n";

?>
