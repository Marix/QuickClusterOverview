<?php
/* 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$a = 1;

function foo( &$b ) {
	$c =& $b;
	$c++;
}

foo ( $a );
echo $a . "\n";

$a = 1;

function ba( &$b ) {
	$c = $b;
	$c++;
	$b = $c;
}

ba ( $a );
echo $a . "\n";

function f1( &$c ) {
	$c++;
}

function f2( &$b ) {
	f1($b);
}

$a = 1;
f2($a);
echo $a . "\n";

?>
