#!/usr/bin/php
<?php
/**
 * RTCO command line client
 *
 * A commandline client that allows accessing the engine the
 * same way the web ui would.
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

// go to document root to avoid strang behaviour due to wrong path
chdir('../www');
require_once('./contrib/timer.php');
$scriptTimer = new Timer();

// parse commandline
$argc = $_SERVER['argc'];
$argv = $_SERVER['argv'];

if( $argc < 2 ) {
	$action = 'get';
} else {
	$action = $argv[1];
}

switch( $action ) {

	case 'get':
 
		$engineTimer = new Timer();
		// load engine
		require_once('./inc/engine.php');
		// and get data
		$data = getData();
		
		$engineTimer->stop();
		
		// print the data for aestetic pleasure
		if( $data === NULL ) {
			echo getError() . "\n";
		} else {
			//print_r( $data );
		}
	
		break;
		
	case 'drop':
	
		$engineTimer = new Timer();
		// load engine
		require_once('./inc/engine.php');
		// and drop data
		if( ! dropData() )
			echo getError() . "\n";
			
		$engineTimer->stop();
		
		break;
	
	case 'showConfig':
	
		$engineTimer = new Timer();
		// load engine
		require_once('./inc/engine.php');
		// and drop data
		$data = getConfig();
		
		$engineTimer->stop();
		
		// print the data for aestetic pleasure
		if( $data === NULL ) {
			echo getError() . "\n";
		} else {
			print_r( $data );
		}
		
		break;
		
	case 'setConfig':
	
		// load config from file given as param
		$file = $argv[2];
		if( ! isset( $file ) ) {
			echo "You did not specify a file containing the new configuration.\nUsage: cmdcli setConfig <file>\n";
			break;
		}
		
		include( $file );
		if( ! isset ( $BASECONFIG ) ) {
			echo "The configuration file did not contain a variable called \$BASECONFIG.\n";
			break;
		}
		$engineTimer = new Timer();
		// load engine
		require_once('./inc/engine.php');
		// and drop data
		if( ! setConfig( $BASECONFIG ) )
			echo getError() . "\n";
		
		$engineTimer->stop();
		
		break;
		
	case 'showPlugins':
	
		// check whether type is limited
		$type = $argv[2];
		
		$engineTimer = new Timer();
		// load engine
		require_once('./inc/engine.php');
		
		// get data plugins from the engine
		if( !isset( $type ) || $type == 'data' ) {
			$plugins = getDataProviderPlugins();
			if( $plugins == NULL ) {
				echo getError() . "\n\n";
			} else {
				foreach( $plugins as $plugin ) {
					echo $plugin['id'] . ': ' . $plugin['name'] . "\n";
					echo $plugin['description'] . "\n\n";
				}
			}
		}
		
		// get data plugins from the engine
		if( !isset( $type ) || $type == 'plot' ) {
			$plugins = getPlotPlugins();
			if( $plugins == NULL ) {
				echo getError() . "\n\n";
			} else {
				foreach( $plugins as $plugin ) {
					echo $plugin['id'] . ': ' . $plugin['name'] . "\n";
					echo $plugin['description'] . "\n\n";
				}
			}
		}

		$engineTimer->stop();
		
		break;
		
	default:
		if( $action != help ) 
			echo "Unknown command $action\n";
		
		echo "Usage: cmdcli.php [command] [file|type]\n";
		echo "\n";
		echo "Possible value of <command>\n";
		echo "\n";
		echo "get          Retrieves the data from the cluster\n";
		echo "             This is the default command.\n";
		echo "\n";
		echo "showConfig   Shows the current configuration\n";
		echo "\n";
		echo "setConfig    Sets the configuration to the value\n";
		echo "             given for \$BASECONFIG in file\n";
		echo "\n";
		echo "drop         Dumps the shared data store\n";
		echo "\n";
		echo "showPlugins  Shows the available plugins. It type\n";
		echo "             is given as data or plot those are shown.\n";
		echo "\n";
	
}	

// showtime ;)
echo "Script execution :     " . $scriptTimer->getTime() . " s\n";
if( isset( $engineTimer ) ) {
	echo "Time spent in engine : " . $engineTimer->getTime() . " s\n";
}
?>
