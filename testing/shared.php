<?php
/**
 * A test for the shared interface
 *
 * @author Matthias Bach <marix (at) marix (dot) org>
 * @version 0.1
 * @since 0.1
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
chdir('../www');
require_once('./inc/shared.php');
 
// parse commandline

// parse params
$argc = $_SERVER['argc'];
$argv = $_SERVER['argv'];
if( $argc < 2 ) {
	$action = 'help';
} else {
	$action = $argv[1];
}

// init shared memory (not needed in help case, but this is a test only, so
// who cares

if( ! shared_init() ) {
	echo $error . "\n";
	$action = 'none';
}

switch( $action ) {
 
 	case 'put':
 		
		// prepare params for being stored
		// this concatination allows easy insertion of normal text
		$data = '';
		if( $argc > 2 )
			$data .= $argv[2];
		for( $i = 3; $i < $argc; $i++ ) {
			$data .= ' ' . $argv[$i];
		}
		
		// put in store
		if( ! shared_put( $data ) )
			echo $error . "\n"; 
		
		break;
		
		
		
	case 'get':
		
		// retrieve and print
		if( shared_get( $data ) )
			echo $data . "\n";
		else
			echo $error . "\n";
		
		break;
		
		
			
	case 'drop':
		
		// let it go
		if( ! shared_drop( ) ) 
			echo $error;		
		
		break;

	case 'none':
		// only for internal use in case of error on startup
		break;
		
		
	default:
		// I hate having to memorize commands
		if( $action != 'help' )
			echo 'Unknown command ' . $action . "\n";
		
		echo "Usage: shared.php <command> [data]\n";
		echo "\n";
		echo "Possible values of <command>:\n";
		echo "put    Store the data in the shared storage. In that case\n";
		echo "       the data argument is mandatory\n";
		echo "get    Retrieves the data from the shared storage and prints\n";
		echo "       it to stdout\n";
		echo "drop   Drops the shared data storage\n";
		echo "help   Displays this help\n";
		echo "\n";
}

shared_done();

?>
