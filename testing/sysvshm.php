<?php

/* sysvshm.php
 *
 * A short testing program for SysV shared memory usage.
 * 
 * @author Matthias Bach
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

$start = microtime();

// get an id for the memory based on the filename of this script
$SHM_KEY = ftok( __file__, chr( 42 ) );

// attach to the shared memory
// will be created if it does not exist
// optionally size and access rights can be given
// in productive they should be 600 probably
$MEM_HANDLE = shm_attach( $SHM_KEY );
if ( ! $MEM_HANDLE ) {
	echo "Failed to attach to shared memory.\n";
	die;
}

if( ( $_SERVER['argc'] == 2 ) ) {
	if ( $_SERVER['argv'][1] == "cleanup" ) {
		if( shm_remove( $MEM_HANDLE ) ) {
			die;
		} else {
			echo "Failed to remove shared memory.\n";
			die;
		}
	} else {
		echo "Invalid parameter, usage is sysvshm.php [cleanup].\n";
	}
}

// static key for the data
$DATA_KEY = "1";

// try to retrieve data from shared memory
$test = @shm_get_var( $MEM_HANDLE, $DATA_KEY );

// test wheter data was set ( string false will not match bool false with this comparator)
if( $test !== false ) {
	echo "Shared memory was accessed before at " . $test[1] . ".\n";
} 

$test = array( time(), date('Y-m-d H:i:s') );

// put data into shared memory
if( ! shm_put_var( $MEM_HANDLE, $DATA_KEY, $test ) ) {
	echo "Error updating shared memory.\n";
	die;
}

// detach from shared memory, it is still there
shm_detach( $MEM_HANDLE );

$end = microtime();

$start = $start_msec + $start_sec;
$end = $end_msec + $end_sec;

echo "Script execution took " . round( ( $end - $start ) , 2 ) . " s.\n";


?>
