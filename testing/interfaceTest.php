#!/usr/bin/php
<?php
/**
 * A test of the the interface definition compliance
 *
 * This script take a couple of pluging and tests whether
 * the implement the interface.
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
chdir( '../www/' );
 
// parse params
$argc = $_SERVER['argc'];
$argv = $_SERVER['argv'];
if( $argc < 2 ) {
	$action = 'help';
} else {
	$action = $argv[1];
}

switch( $action ) {

	case 'plot':
	
		$successfull = 0;
		for( $i = 2; $i < $argc; $i++ ) {
			
			$plugin = $argv[$i];
			
			echo 'Checking ' . $plugin;
			
			require_once( './plotter/' . $plugin . '.php' );
		
			if( check_function( $plugin, 'getDescription' ) )
				continue;
			if( check_function( $plugin, 'getVersion' ) )
				continue;
			if( check_function( $plugin, 'getAuthor' ) )
				continue;
			if( check_function( $plugin, 'getInterfaceVersion' ) )
				continue;
			$funcName = $plugin . '_getInterfaceVersion';
			$pluginIntVersion = $funcName();
			if( $pluginIntVersion != '0.1' ) {
				echo " failed.\nPlugin interface version is $pluginIntVersion instead of 0.1";
				continue;
			}
			if( check_function( $plugin, 'plot' ) )
				continue;
			if( check_function( $plugin, 'getError' ) )
				continue;
			$successfull++;
			echo " success\n";
		}

		echo $successfull . ' of ' . ( $argc - 2 ) . " plugins where testes successfull.\n";
		break;
		
	case 'data':
	
		$successfull = 0;
		for( $i = 2; $i < $argc; $i++ ) {
			
			$plugin = $argv[$i];
			
			echo 'Checking ' . $plugin;
			
			require_once( './datasources/' . $plugin . '.php' );
		
			if( check_function( $plugin, 'getDescription' ) )
				continue;
			if( check_function( $plugin, 'getVersion' ) )
				continue;
			if( check_function( $plugin, 'getAuthor' ) )
				continue;
			if( check_function( $plugin, 'getInterfaceVersion' ) )
				continue;
			$funcName = $plugin . '_getInterfaceVersion';
			$pluginIntVersion = $funcName();
			if( $pluginIntVersion != '0.1' ) {
				echo " failed.\nPlugin interface version is $pluginIntVersion instead of 0.1";
				continue;
			}
			if( check_function( $plugin, 'getData' ) )
				continue;
			if( check_function( $plugin, 'getError' ) )
				continue;
			if( check_function( $plugin, 'requiresClientList' ) )
				continue;
				
			$successfull++;
			echo " success\n";
		}

		echo "\n" . $successfull . ' of ' . ( $argc - 2 ) . " plugins where testes successfull.\n";
		break;
	
	default:
		if( $action != 'help' )
			echo 'Unknown type ' . $action . "\n";
		
		echo "Usage: interfaceTest.php <type> [plugin ...]\n";
		echo "\n";
		echo "Possible values of <type>:\n";
		echo "data : Checks a data provider plugin\n";
		echo "plot : Checks a plot provider plugin\n";
		echo "help : Displays this help\n";
		echo "\n";
}

/**
 * Function tests whether a function does not exist in the plugin
 *
 * @return FALSE if the function exists, TRUE if it does NOT
 */
function check_function( $plugin, $name ) {

	$funcName = $plugin . '_' . $name;
	$failed = ! function_exists( $funcName );
	
	if( $failed )
		echo " failed\n$funcName() does not exist.\n";
			
	return $failed;
}
?>
