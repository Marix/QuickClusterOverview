<?php
/**
 * A short testprogram for parsing ganglia xml-files.
 * 
 * Parsing the xml is currently done using the standard expat based xml parser provided
 * by php. This parser is the only php-xml-parser that by default available in php4.
 * In addition this parser is also used by ganglia and seems to be able to provide good performance.
 *
 * @author Matthias Bach <marix@marix.org>
 * @version 0.1
 * @since 0.1
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
// enable time measurement
require_once( "../contrib/timer.php" );
// measure overal script time
$scriptTimer = new Timer();

$GANGLIA_XML_ELEM_CLUSTER = "CLUSTER";
$GANGLIA_XML_ELEM_GRID = "GRID";
$GANGLIA_XML_ELEM_HOST = "HOST";
$GANGLIA_XML_ELEM_METRIC = "METRIC";
$GANGLIA_XML_ELEM = "GANGLIA_XML";

// the file that will currently be used for testing
//$GANGLIA_FILE = "data/paderborn-data.dat";

// the source to read the data from
$HOST = "localhost";
$HOST_PORT = 8649;

// how much data to read in one chunk
$CHUNK_SIZE = 4096;

// whether to show the parsing results
$OUTPUT = TRUE; 

// hostIdx actually contains the number of hosts - 1
$hostIdx = -1;


// measure actual parsing time
$parseTimer = new Timer();

$dataSet = array();

$metrics = array( 'cpu_sys', 'cpu_user', 'cpu_nice' );

// create and initialize the xml-parser
$xml_parser = xml_parser_create( );
// might be wise to explicitly set the target encoding
// php internally uses UTF-8 so this can safe some conversion
// foolish however if everything is is ISO-8859-1.
//if( ! xml_parser_set_option( XML_OPTION_TARGET_ENCODING, "UTF-8") ) {
//	die( "Failed to set target encoding: " . xml_error_string( xml_get_error_code() ) );
//}

// register element handler
xml_set_element_handler( $xml_parser, "start_element", "end_element" );

// open the file containing the data for reading
//$fp = fopen( $GANGLIA_FILE, "r" );
// open the server to read the data from
$fp = fsockopen( $HOST, $HOST_PORT, $errno, $errstr, 30 );
if( !$fp )
	die( "Failed to retrieve ganglia xml: " . $errstr . " (". $errno . ")\n" );

// read chunks of data from the file and fead them to the parser
while( $datachunk = fread( $fp, $CHUNK_SIZE ) ) {
	if( ! xml_parse( $xml_parser, $datachunk, feof( $fp ) ) ) {
		die( "Failed to parse the ganlia xml file: " . xml_error_string( xml_get_error_code( $xml_parser ) ) . " in line " . xml_get_current_line_number( $xml_parser ) . " \n" );
	}
}

fclose( $fp );

$parseTimer->stop();
// output data for control

if( $OUTPUT ) {
	foreach( $dataSet as $metric ) {
		echo $metric['id']; echo ":\n";
		foreach( $metric['NODES'] as $host ) {
			echo $host['name'] . ": " . $host['value'] . "\n";
		}
		echo "\n";
	}
}

$scriptTimer->stop();

echo "Script execution : " . $scriptTimer->getTime() . " s\n";
echo "Parsing :          " . $parseTimer->getTime() . " s\n";
echo "Number of hosts :  " . ( $hostIdx + 1 ) . "\n";

/**
 * Function handles start of tags
 */
function start_element( $parser, $element, $attribs ) {
	global $hostIdx;
	global $grid;
	global $cluster;
	global $hostIP;
	global $hostName;
	global $dataSet;
	global $metrics;
	
	global $GANGLIA_XML_ELEM_GRID;
	global $GANGLIA_XML_ELEM_METRIC;
	global $GANGLIA_XML_ELEM_HOST;
	global $GANGLIA_XML_ELEM_CLUSTER;
	global $GANGLIA_XML_ELEM;
	
	switch( $element ) {
		case $GANGLIA_XML_ELEM_GRID:
			$grid = $attribs['NAME'];
			break;
		case $GANGLIA_XML_ELEM_CLUSTER:
			$cluster = $attribs['NAME'];
			break;
		case $GANGLIA_XML_ELEM_HOST:
			$hostIP = $attribs['IP'];
			$hostName = $attribs['NAME'];
			$hostIdx++;
			break;
		case $GANGLIA_XML_ELEM_METRIC:
			$metricName = $attribs['NAME'];
			if( in_array( $metricName, $metrics ) ) {
				$metric = &$dataSet[ $metricName ];
				$metric['name'] = $metricName;
				$metric['id'] = $metricName;
				$metric['NODES'][ $hostIdx ]  = array( 'id' => $hostIP, 'name' => $hostName, 'value' => $attribs['VAL'] );
			}
			break;
		case $GANGLIA_XML_ELEM;
			break;
		default:
			echo "Unknown element: " . $element . "\n";
	}
}
 
function end_element( $parser, $element ) {

// 	global $hostIdx;
// 	global $grid;
// 	global $cluster;
// 	global $host;
// 	
// 	switch( $element ) {
// 		case GANGLIA_XML_GRID:
// 			unset($grid);
// 			break;
// 		case GANGLIA_XML_CLUSTER:
// 			unset($cluster);
// 			break;
// 		case GANGLIA_XML_HOST:
// 			unset($host);
// 			break;
// 		// default ignore
// 	}
}
?>
