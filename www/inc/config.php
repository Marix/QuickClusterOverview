<?php
/**
 * Configuration handling
 *
 * This file contains basic functions for handling checking,
 * reading and writing the configuration.
 *
 * @author Matthias Bach
 * @since 0.1
 * @version 0.2
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
/*
 * Some required vars that should not be modified at runtime
 */
 
// gnuplot command if gnuplot plugin is used (otherwise not needed)
global $GNUPLOT;
$GNUPLOT = "gnuplot";  // for linux 
 
// size for the shared memory in bytes
global $SHARED_MEM_SIZE;
$SHARED_MEM_SIZE = 250 * 1024;

// where the persisten configuration is stored
global $BASECONFIG_FILE;
$BASECONFIG_FILE = './configs/baseconfig.php';
 
 
/**
 * Checks whether the given variable contains data.
 * If it doesn't then the configuration is parsed 
 * from the configuraiton file.
 *
 * In case of error writes to $error.
 *
 * @return TRUE on success, FALSE on failure
 */
function config_check( &$data ) {
	global $error;
	global $BASECONFIG_FILE;

	// if configuration is already maintained nothing needs to be done	
	if( isset( $data['CONFIG'] ) )
		return TRUE;

	// otherwise read from files
	include( $BASECONFIG_FILE );
	
	if( ! isset( $BASECONFIG ) ) {
		$error = 'Failed to read configuration from baseconfig.php';
		return FALSE;
	}
	
	$data['CONFIG'] = $BASECONFIG;
	return TRUE;
}

/**
 * Writes the configuration into the base config file
 */
function config_write( $data ) {
	global $BASECONFIG_FILE;
	include( 'baseconfigWriter.php' );
	return config_writeBaseConfig( $data['CONFIG'], $BASECONFIG_FILE );
}

/**
 * A quick utility that checks whether a string variable is set to true
 */
function is_true( $var ) {
	return (boolean) ( isset( $var ) && ( (boolean) $var ) && $var != 'false' ); 
}

?>
