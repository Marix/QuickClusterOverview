<?php
/**
 * Shared data engine
 *
 * Here access to the shared data is implemented.
 * Note that while access to the shared data from different
 * applications using these methods is threadsafe, it is not
 * within one usage of this file.
 *
 * @author Matthias Bach
 * @since 0.1
 * @version 0.1 
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
/*
 * variables
 */
 
$shared_sem_handle = FALSE;
$shared_sem_aquired = FALSE;
$shared_shm_handle = FALSE; 

// static key for the data in the store
$DATA_KEY = '42';
 
/**
 * Inits access to the shared data
 * In case of error writes to $error
 *
 * @return TRUE on success, FALSE on failure.
 */
function shared_init() {
	global $error;
	global $shared_sem_handle;
	global $shared_shm_handle;
	global $shared_sem_acquired;
	
	global $SHARED_MEM_SIZE;
	
	//get a unique key for shared access
	$shared_key = ftok( __FILE__, chr( 42 ) );
	if( $shared_key == -1 ) {
		$error = 'Failed to created identifier for shared resources.';
		return FALSE;
	}
	
	//get semaphore
	if( ! ($shared_sem_handle = sem_get( $shared_key ) ) ) {
		$error = 'Failed to get semaphore for shared resources';
		return FALSE;
	};
	
	//attach to shared memory
	if( ! ($shared_shm_handle = shm_attach( $shared_key, $SHARED_MEM_SIZE ) ) ) {
		$error = 'Failed to attach to shared memory';
		return FALSE;
	};
	
	//lock it for accessing shared res, just in case we want to modify
	$shared_sem_acquired = sem_acquire( $shared_sem_handle );
	if( ! $shared_sem_acquired ) {
		$error = 'Unable to acquire lock on shared resources.';
		return FALSE;
	}
	
	return TRUE;
}

/**
 * This function always has be to called if init was called and the
 * shared data is no longer needed.
 */
function shared_done() {
	global $shared_sem_acquired;
	global $shared_sem_handle;
	global $shared_shm_handle;

	if( $shared_sem_acquired )
		sem_release( $shared_sem_handle );
		
	if( $shared_shm_handle )
		shm_detach( $shared_shm_handle );
}

/**
 * Tries to recieve the data from shared memory
 * and writes it to the var given in the argument
 * Writes to $error in case of error.
 * If the data has never been written the method
 * returns successfull, but without writing to 
 * the parameter. This is usuefull to not get
 * error messages on startup.
 *
 * Access to this function is possible without
 * previously locking, but beware of the implications.
 *
 * @return TRUE on success or FALSE on failure
 */
function shared_get( &$data ) {
	global $shared_shm_handle;
	global $DATA_KEY;
	
	$tmp = @shm_get_var( $shared_shm_handle, $DATA_KEY );
	
	if( $tmp !== FALSE ) // if it's identical false it has never been written
		$data = $tmp;    // at least if we assume we never write boolean FALSE ;)
		
	return TRUE;
}

/**
 * Stores the data in shared mem.
 * Write to $error in case of error.
 *
 * @return TRUE on success, FALSE on failure
 */
function shared_put( $data ) {
	global $error;
	global $shared_sem_acquired;
	global $shared_shm_handle;
	global $DATA_KEY;
	global $SHARED_MEM_SIZE;

	if( ! $shared_sem_acquired ) {
		$error = 'Shared memory not locked. Shared_init() has probably not been called';
		return FALSE;
	}
	
	// put data into shared memory
	if( ! shm_put_var( $shared_shm_handle, $DATA_KEY, $data ) ) {
		$error = 'Failed to put data into shared memory.';
		$error .= ' Current memory size is ' . $SHARED_MEM_SIZE . ' bytes. At least ' . 2 * strlen( serialize( $data ) ) . ' bytes is required.';
		$error .= ' You can adjust the shared memory size by setting $SHARED_MEM_SIZE in inc/config.php.';
		$error .= ' After changing $SHARED_MEM_SIZE in inc/config.php you will have to drop the shared data storage for the changes to take effect.';
		return FALSE;
	}
	
	return TRUE;
}

/**
 * Drops any non-persistent data in the datastore
 * Write to $error in case of error.
 *
 * @return TRUE on success, FALSE on failure
 */
function shared_drop( ) {
	global $error;
	global $shared_sem_acquired;
	global $shared_shm_handle;

	if( ! $shared_sem_acquired ) {
		$error = 'Shared memory not locked. Shared_init() has probably not been called';
		return FALSE;
	}
	
	if( ! shm_remove( $shared_shm_handle ) ) {
		$error = 'Failed to remove shared memory';
		return FALSE;
	}
	
	// shared memory removed, mark as such
	$shared_shm_handle = FALSE;
	return TRUE;
}
?>
