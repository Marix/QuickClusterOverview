<?php
/**
 * The engine
 *
 * This is where it's all tied toghether. From here the data retrieval plugins
 * are queried, shared storage is queried and updated and the plot plugins are
 * called.
 *
 * @author Matthias Bach
 * @since 0.1
 * @version 0.1
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
require_once( './inc/shared.php' );
require_once( './inc/config.php' );
 
/**
 * Contains an error string in case something went wrong
 */
$error;

/**
 * The main function.
 *
 * On call it recieves the data from the cluster, if necessary, and returns it to
 * the caller. In addition it also has the data plotted as configured so that the
 * caller can then reference those plots.
 *
 * @return The data structure or NULL in case of error.
 * @since 0.1
 * @version 0.2
 */
function getData() {
	global $error;

	if( !shared_init() ) {
		shared_done();
		return NULL;
	}
	
	// retrieve data from shared store ( currently this cannot fail );
	shared_get( $data );
	
	// make sure we have a valid config
	if( ! config_check( $data ) ) {
		shared_done();
		return NULL;
	}
	
	// make sure we have valid cluster data
	if( ! engine_checkClusterData( $data ) ) {
		shared_done();
		return NULL;
	}
	
	// put data back into shared data perisstence
	if( ! shared_put( $data ) ) {
		shared_done();
		return NULL;
	}
	
	// now we should be done with everything that effects others as well	
	shared_done();
	
	// and finally we can return our doings to the caller
	return $data;
}

/**
 * Retrieves config only.
 *
 * @return The config data or NULL on error
 */
function getConfig() {
	global $error;

	if( !shared_init() ) {
		shared_done();
		return NULL;
	}
	
	// retrieve data from shared store ( currently this cannot fail );
	shared_get( $data );
	
	// make sure we have a valid config
	if( ! config_check( $data ) ) {
		shared_done();
		return NULL;
	}

	shared_done();
	return $data['CONFIG'];
}

/**
 * Stores the new config
 *
 * @return The config data or NULL on error
 */
function setConfig( $config ) {
	global $error;

	if( !shared_init() ) {
		shared_done();
		return FALSE;
	}
	
	// retrieve data from shared store ( currently this cannot fail );
	shared_get( $data );
	
	$data['CONFIG'] = $config;
	
	// make sure we have a valid config
	if( ! config_write( $data ) ) {
		shared_done();
		return FALSE;
	}

	// put data back into shared data perisstence
	if( ! shared_put( $data ) ) {
		shared_done();
		return FALSE;
	}
	
	shared_done();
	return TRUE;
}


/**
 * Drops all the data currently stored in non persisten shared
 * storage. This will case a refresh of the configuration and
 * cluster data on the next get. Repeatedly calling this will
 * lead to performance breakdown.
 *
 * @return TRUE on success, FALSE on failure
 */
function dropData() {
	
	// make sure no one's writing
	if( !shared_init() ) {
		shared_done();
		return FALSE;
	}

	// drop
	if( !shared_drop() ) {
		shared_done();
		return FALSE;
	}
	
	// and let everbody do what he wants
	shared_done();
	return TRUE;
}

/**
 * Returns a human readable error string in case there was one.
 */
function getError() {
	global $error;
	return $error;
}

/**
 * Returns a list of plugins that can be used for plotting.
 * Each list item contaings id, name and description of the
 * plugin indexed by those id, name and description.
 * 
 * In case of error writest to $error.
 *
 * @return a list of plugins, NULL on failure
 */
function getPlotPlugins() {
	return engine_getPlugins( './plotter' );
}

/**
 * Returns a list of plugins that can be used for pulling data.
 * Each list item contaings id, name and description of the
 * plugin indexed by those id, name and description.
 * 
 * In case of error writest to $error.
 *
 * @return a list of plugins, NULL on failure
 */
function getDataProviderPlugins() {
	return engine_getPlugins( './datasources' );
}

/**
 * Returns a list of stylesheets that can be used.
 *
 * In case of error writes to $error
 *
 * @return a list of stylesheets, NULL on failure
 */
function getStylesheets() {
	// open the directory for reading
	$dh = @opendir( '.' );
	if( $dh === FALSE ) {
		$error = "Unable to open stylesheet directory '.'";
		return NULL;
	}
	
	$stylesheets = array();
	while(( $file = readdir( $dh ) ) ) {
		// drop out everything that is not a php-file
		if( substr( $file, -4 ) == '.css' ) {
			$stylesheets[] = $file;
		}
	}
	
	// clean up
	closedir( $dh );
	
	return $stylesheets;
}

/**
 * Resolves the stylesheet to use
 *
 * @return a string containing the stylesheet name
 */
function resolveStylesheet( $CONFIG = NULL ) {
	$fallback = 'modern.css';

	if( !isset( $CONFIG ) || $CONFIG == NULL ) {
		return $fallback;
	}
	$stylesheet = $CONFIG['stylesheet'];
	if( !isset( $stylesheet ) || $stylesheet == '' ) {
		$stylesheet = $fallback;
	}
	return $stylesheet;
}

/*
 * Internal functions that should not be called from the outside
 */
 
/**
 * Checks whether the data recieved is still up to date
 * and refreshs if in doubt. Also refreshs the plots.
 *
 * In case of error writes to $error. 
 *
 * @return TRUE on success, FALSE on failure
 */
function engine_checkClusterData( &$data ) {
	// get the current time, for checks and writebacks
	$now = time();
	
	// only if time the intervall has passed since the last update we need to refresh
	if( isset( $data['CLUSTER_DATA'] ) && $now < ( $data['CLUSTER_DATA']['timestamp'] + $data['CONFIG']['intervall'] ) ) {  // if timestamp is not set, e.g. nothing read yet, the right hand side should evaluate to $CONFIG['intervall] only, therefore the boolean expression will be false
		// we are good, no update needed
		return TRUE;
	}
	
	// pull new data from cluster (plugin)
	if( ! engine_refreshClusterData( $data ) ) {
		return FALSE;
	}
	
	// plot the new data
	if( ! engine_plotData( $data ) ) {
		return FALSE;
	}
	
	// we are done, the data has been refreshed. for the next intervall we can save ourselfes the hassle
	$data['CLUSTER_DATA']['timestamp'] = $now;
	return TRUE;
}

/**
 * Refreshs the data from the cluster.
 *
 * Reads which plugin to use and then loads and invokes it to
 * get fresh information about the cluster status.
 *
 * In case of error writes to $error.
 *
 * @return TRUE on success, FALSE on error.
 */
function engine_refreshClusterData( &$data ) {
	global $error;
	
	// just for convinience
	$CONFIG = $data['CONFIG'];
	
	// create everything for plugin invocation
	$plugin = $CONFIG['DATASOURCE']['name'];
	$pluginFile = './datasources/' . $plugin . '.php';
	$getDataMethod = $plugin . '_getData';
		
	// check whether plugin is available
	include_once( $pluginFile );
	if( ! function_exists( $getDataMethod ) ) {
		$error = 'Data retrival plugin ' . $plugin . ' could not be loaded.';
		return FALSE;	
	}

	// figure out where we are. we should be one set after the last one queried, but wrap around before reaching the end (this is round robin after all)
	if( !isset( $data['CLUSTER_DATA']['currentSet'] ) ) {
		$currentSet = 0;
	} else {
		$currentSet = ($data['CLUSTER_DATA']['currentSet'] + 1) % $data['CONFIG']['maxHistory'];
	}
	$data['CLUSTER_DATA']['currentSet'] = $currentSet;
		
	// prepare list of metrics for filling by plugin
	foreach( $CONFIG['GRAPHS'] as $GRAPH ) {
		$METRICS = explode( '|', $GRAPH['metric'] );
		foreach( $METRICS as $metric ) {
			$data['CLUSTER_DATA'][ 'DATA_SETS' ][$currentSet][$metric] = array();
		}
	}

	// invoke the plugin to get the data
	if( ! $getDataMethod( $data['CLUSTER_DATA'][ 'DATA_SETS' ][$currentSet], $CONFIG ) ) {
		$getNameMethod = $plugin . "_getName";
		$getErrorMethod = $plugin . "_getError";
		$error = 'Retrieving data from ' . $getNameMethod() . ' failed: ' . $getErrorMethod();
		return FALSE;
	}
	
	// we retrieved the data. Lets hope it gets where it's supposed to be
	return TRUE;
}

/**
 * Refreshs the plotted data.
 *
 * Loads the configured plot plugin and invokes it to plot the
 * current data.
 *
 * In case of error writes to $error.
 *
 * @return TRUE on success, FALSE on error.
 */
function engine_plotData( $data ) {
	global $error;
	
	// just vor convinience
	$CONFIG = $data['CONFIG'];
	$CLUSTER_DATA = $data['CLUSTER_DATA'];
	
	// figure out which plugin to use
	$plugin = $CONFIG['plotPlugin'];
	
	// prepare plugin usage
	$pluginFile = './plotter/' . $plugin . '.php';
	$plotFunc = $plugin . '_plot';
	
	// load plugin and check availablity
	// check whether plugin is available
	include_once( $pluginFile );
	if( ! function_exists( $plotFunc ) ) {
		$error = 'Plot provider plugin ' . $plugin . ' could not be loaded.';
		return FALSE;	
	}
	
	// invoke plugin
	if( ! $plotFunc( $CLUSTER_DATA, $CONFIG ) ) {
		$getNameMethod = $plugin . "_getName";
		$getErrorMethod = $plugin . "_getError";
		$error = 'Plotting data using ' . $getNameMethod() . ' failed: ' . $getErrorMethod();
		return FALSE;
	}
	
	return TRUE;
}

/**
 * Returns a list of plugins contained in the given directory.
 * Each list item contaings id, name and description of the
 * plugin indexed by those id, name and description.
 * The directory passed may not contain a trailing slash.
 * 
 * In case of error writest to $error.
 *
 * @return a list of plugins, NULL on failure
 */
function engine_getPlugins( $directory ) {
	
	// open the directory for reading
	$dh = @opendir( $directory );
	if( $dh === FALSE ) {
		$error = "Unable to open plugin directory $directory";
		return NULL;
	}
	
	$plugins = array();
	$i = 0;
	while(( $file = readdir( $dh ) ) ) {
		// drop out everything that is not a php-file
		if( substr( $file, -4 ) == '.php' ) {
			// try to include and get data
			$plugin = substr( $file, 0, -4 );
			$getNameFunction = $plugin . '_getName';
			$getDescriptionFunction = $plugin . '_getDescription';
			$getAuthorFunction = $plugin . '_getAuthor';
			$getVersionFunction = $plugin . '_getVersion';
			
			include_once( $directory . '/' . $file );
			
			$plugins[$i]['id'] = $plugin;
			$plugins[$i]['name'] = $getNameFunction();
			$plugins[$i]['description'] = $getDescriptionFunction();
			$plugins[$i]['author'] = $getAuthorFunction();
			$plugins[$i]['version'] = $getVersionFunction();
			
			$i++;
		}
	}
	
	// clean up
	closedir( $dh);
	
	return $plugins;
}

?>
