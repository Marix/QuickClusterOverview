<?php
/**
 * Basic configuration file writer utility
 *
 * This file contains a utility method for writing a php file
 * containing the base config. Including this only if needed
 * will save the trouble of parsing this rather big spaghetti-code
 * everytime.
 *
 * @author Matthias Bach
 * @since 0.1
 * @version 0.1
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
// the definition of the config file
// it's an array containing sections. Each section array containg a comment and
// a list of entries. Each entry contains a comment and an array called path, giving
// the path in the config file to this array. if a path item is called _idx_ then it 
// will be iterated over. it can also contain a default-entry which
// will be used for a commented out entry if the entry is not set in the config to be
// written.
 
global $CONFIG_DEF;
$CONFIG_DEF = array( 
		array( 'comment' => 'bevaviour configuration',
			'entries' => array( 
				array( 'comment' => 'update intervall',
					'path' => array( 'intervall' ),
					'default' => 5 ),
				array( 'comment' => 'how many datasets shall be held at the same time',
					'path' => array( 'maxHistory' ),
					'default' => 1 ),
				array( 'comment' => 'Whether to show the list of nodes on the frontpage. Possible values are show, optional and none. If not set it defaults to optional.',
					'path' => array( 'showNodeList' ),
					'default' => 'optional' ),
				array( 'comment' => 'The stylesheet to use. If omitted hardcoded default will be used.',
					'path' => array( 'stylesheet' ),
					'default' => 'default.css'),
				array( 'comment' => 'If set to any value other than false, an emtpy string or zero (or not at all) it will not be possible to edit the configuration via the ui. All configuration changes will have to be made via the baseconfig file and loaded into the application using the cmdcli in the testing directory.',
					'path' => array( 'disableConfigUI' ),
					'default' => 'false' )
				)
			),
			
		array( 'comment' => 'plot configuration',
			'entries' => array( 
				array( 'comment' => 'which plugin to use for plotting',
					'path' => array( 'plotPlugin' ),
					'default' => 'gnuplot' ),
				array( 'comment' => 'where to put the plots (make sure to include the traling slash). keep in mind that this directory has to be writeable by the webserver.',
					'path' => array( 'graphBasePath' ),
					'default' => './graphs/' )
				)
			),
			
		array( 'comment' => 'some default values that can be overriden for single plots',
			'entries' => array(
				array( 'comment' => 'The minimum value for the y-range. Can be set to auto for autoscaling',
					'path' => array( 'ALLGRAPHS', 'YRANGE', 'min' ),
					'default' => '0' ),
				array( 'comment' => 'The maximum value for the y-range. Can be set to auto for autoscaling',
					'path' => array( 'ALLGRAPHS', 'YRANGE', 'max' ),
					'default' => 'auto' ),
				array( 'comment' => 'The size in x-direction that shall be used for all graphs that do not have an own value given',
					'path' => array( 'ALLGRAPHS', 'SIZE', 'x' ),
					'default' => '1' ),
				array( 'comment' => 'The size in y-direction that shall be used for all graphs that do not have an own value given',
					'path' => array( 'ALLGRAPHS', 'SIZE', 'y' ),
					'default' => '.5' ),
				array( 'comment' => 'If set to any value other than false, an emtpy string or zero (or not at all) the graphs will be plotted in a filled style. This might not work with older version of gnuplot, so.',
					'path' => array( 'ALLGRAPHS', 'fill' ),
					'default' => '' )
			)
		), 
		
		array( 'comment' => 'definition of each plot that shall be made',
			'entries' => array(
				array( 'comment' => 'The title to be used for the plot',
					'path' => array( 'GRAPHS', '_idx_', 'title' ),
					'default' => 'Title' ),
				array( 'comment' => 'The metric that will be used for this graph. You can specify multiple metrics seperated by a vertical bar like "cpu_nice|cpu_user|cpu_system". This will display the metrics in one graph on top of each other, with the first metric given being the uppermost.',
					'path' => array( 'GRAPHS', '_idx_', 'metric' ),
					'default' => 'foo' ),
				array( 'comment' => 'The name of the file the plot will be stored in below the graphBasePath',
					'path' => array( 'GRAPHS', '_idx_', 'file' ),
					'default' => 'foo.png' ),
				array( 'comment' => 'The minimum value for the y-range. Can be set to auto for autoscaling. If not set global value will be used.',
					'path' => array( 'GRAPHS', '_idx_', 'YRANGE', 'min' ),
					'default' => '0' ),
				array( 'comment' => 'The maximum value for the y-range. Can be set to auto for autoscaling. If not set global value will be used.',
					'path' => array( 'GRAPHS', '_idx_', 'YRANGE', 'max' ),
					'default' => 'auto' ),
				array( 'comment' => 'The size in x-direction that shall be used for the graph. If not set global value will be used.',
					'path' => array( 'GRAPHS', '_idx_', 'SIZE', 'x' ),
					'default' => '1' ),
				array( 'comment' => 'The size in y-direction that shall be used for the graph. If not set global value will be used.',
					'path' => array( 'GRAPHS', '_idx_', 'SIZE', 'y' ),
					'default' => '.5' ),
				)
			),
		
		array( 'comment' => 'definition of where to pull the data from (and which)',
			'entries' => array( 
				array( 'comment' => 'name of the plugin providing the data',
					'path' => array( 'DATASOURCE', 'name' ),
					'default' => 'ganglia' ),
				array( 'comment' => 'The name of the computer the data will be pulled from',
					'path' => array( 'DATASOURCE', 'PROVIDER', '_idx_', 'name' ),
					'default' => 'localhost' ),
				array( 'comment' => 'The port on the remote computer to connect for. Not required by all plugins',
					'path' => array( 'DATASOURCE', 'PROVIDER', '_idx_', 'port' ),
					'default' => '8649' )
				),
				array( 'comment' => 'if the plugin doesn need a list of clients. otherwise we would have to define them here',
					'path' => array( 'DATASOURCE', 'PROVIDER', '_idx_', 'CLIENTS' ),
					'default' => 'foo bla 42' )
			)
		
	);
 
/**
 * Utility function writing the given config to baseconfig.php
 * as a php file for later inclusion as $BASECONFIG.
 *
 * Writes to $error in case of error
 *
 * @return TRUE on success, FALSE if failed.
 */ 
function config_writeBaseConfig( $CONFIG, $target ) {
	global $error;

	// first backup old config (you never know). Of course only backup if target file already exists.
	if( file_exists( $target ) ) {
		$backedUp = rename( $target, $target . '.old.php' );
		if( ! $backedUp ) {
			$error = "Unable to back up $target to $target.old.php. Is the directory writeable?";
			return FALSE;
		}
	}
	
	// open the target for writing
	$fp = fopen( $target, 'w' );
	if( !$fp ) {
		$error = "Unable to write open $target for writing of the configuration. Is the file/directory writeable?";
		config_baseConfigBackupNotice( $target );
		return FALSE;
	}
	
	if( !config_writeBaseConfigHeader( $fp ) ) {
		fclose( $fp );
		config_baseConfigBackupNotice( $target );
		return FALSE;
	}
	
	if( !config_writeBaseConfigContent( $fp, $CONFIG ) ) {
		fclose( $fp );
		config_baseConfigBackupNotice( $target );
		return FALSE;
	}
	
	if( !config_writeBaseConfigFooter( $fp ) ) {
		fclose( $fp );
		config_baseConfigBackupNotice( $target );
		return FALSE;
	}
	
	fclose( $fp );

	return TRUE;
}

/**
 * Tries to play back the backup or writes a note where it can be found.
 */
function config_baseConfigBackupNotice( $target ) {
	global $error;

	if( rename( $target . '.old.php', $target ) ) {
		$error .= " Your old configuration has been restored.";
	} else {
		$error .= " Your old configuration can be found at $target.old.php";
	}
}

/**
 * Writes the php file start and the comment for the config file
 *
 * Writes to $error in case of error.
 *
 * @return TRUE if successfull, FALSE if an error occurs.
 */
function config_writeBaseConfigHeader( $fp ) {
	global $error;

	$text = "<?php\n"
		. "/**\n"
		. "* Basic configuration file\n"
		. "*\n"
		. "* This file contains the basic configuration that will be available even after\n"
		. "* the shared data has been dumped.\n"
		. "*\n"
		. "* Be aware that this file can be overwritten if the data is changed via the\n"
		. "* application user interface.\n"
		. "*\n"
		. "* If you modify this file you have to make sure it's reloaded by the application,\n"
		. "* which it usually isn't during execution. \n"
		. "*\n"
		. "* @author Matthias Bach\n"
		. "* @since 0.1\n"
		. "* @version 0.1\n"
		. "*/\n\n";
	
	
	if( fwrite( $fp, $text ) === FALSE ) {
		$error = 'Failed to write config file.';
		return FALSE;
	}
	
	return TRUE;	
}

/**
 * Writes the php file end
 *
 * Writes to $error in case of error.
 *
 * @return TRUE if successfull, FALSE if an error occurs.
 */
function config_writeBaseConfigFooter( $fp ) {
	global $error;
	
	if( fwrite( $fp, '?>' ) === FALSE ) {
		$error = 'Failed to write config file.';
		return FALSE;
	}
	
	return TRUE;
}

function config_writeChildrenConfig( $prefix, $ENTRY, $SUB_PATH, $comment, $default ) {
	if( ! isset( $ENTRY ) ) {
		// this one definetly has no children ;)
		return '';
	}

	$text = '';
	foreach( $ENTRY as $childKey => $child ) {
		$leaf = $child;
		$pathString = $prefix . '[' . $childKey . ']';
		$offset = 0;
		foreach( $SUB_PATH as $idx ) {
			$offset++;
			if( $idx == '_idx_' ) {
				$text .= config_writeChildrenConfig( $pathString, $leaf, array_slice( $SUB_PATH, $offset ), $comment, $default );
			} else {
				$pathString .= '[\'' . $idx . '\']';
				$leaf = $leaf[ $idx ];
			}
		}
		
		if( isset( $leaf ) ) {
			if( is_string( $leaf ) )
				$text .= '$BASECONFIG' . $pathString . ' = \'' . $leaf . '\'; // ' . $comment . "\n";
			else
				$text .= '$BASECONFIG' . $pathString . ' = ' . $leaf . '; // ' . $comment . "\n";
		} else {
			if( is_string( $default ) )
				$text .= '//$BASECONFIG' . $pathString . ' = \'' . $default . '\'; // ' . $comment . "\n";
			else
				$text .= '//$BASECONFIG' . $pathString . ' = ' . $default . '; // ' . $comment . "\n";
		}
	}
	
	return $text;
}

/**
 * Writes configuration to the php file
 *
 * Writes to $error in case of error.
 *
 * @return TRUE if successfull, FALSE if an error occurs.
 */
function config_writeBaseConfigContent( $fp, $CONFIG ) {
	global $error;
	global $CONFIG_DEF;

	foreach( $CONFIG_DEF as $SECTION ) {
		// write section comment
		$text = '// ' . $SECTION['comment'] . "\n";
		$text .= "\n";
		
		// write the section entries
		foreach( $SECTION['entries'] as $ENTRY ) {
			$PATH = $ENTRY['path'];
			$comment = $ENTRY['comment'];
			$default = $ENTRY['default'];
			$pathString = '';
			$leaf = $CONFIG;
			$offset = 0;
			$written = false;
			foreach( $PATH as $idx ) {
				$offset++;
				if( $idx == '_idx_' ) {
					$text .= config_writeChildrenConfig( $pathString, $leaf, array_slice( $PATH, $offset ), $comment, $default );
					$written = true;
				} else {
					$pathString .= '[\'' . $idx . '\']';
					$leaf = $leaf[ $idx ];
				}
			}
			
			if( ! $written ) {
				if( isset( $leaf ) ) {
					if( is_string( $leaf ) )
						$text .= '$BASECONFIG' . $pathString . ' = \'' . $leaf . '\'; // ' . $comment . "\n";
					else
						$text .= '$BASECONFIG' . $pathString . ' = ' . $leaf . '; // ' . $comment . "\n";
				} else {
					if( is_string( $default ) )
						$text .= '//$BASECONFIG' . $pathString . ' = \'' . $default . '\'; // ' . $comment . "\n";
					else
						$text .= '//$BASECONFIG' . $pathString . ' = ' . $default . '; // ' . $comment . "\n";
				}
			}
		}
		
		$text .= "\n\n";
		
		if( fwrite( $fp, $text ) === FALSE ) {
			$error = 'Failed to write config file.';
			return FALSE;
		}
	
	}
	
	return TRUE;
}

?>
