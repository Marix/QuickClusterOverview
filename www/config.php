<?php
/**
 * Configuration UI
 * 
 * This is the configuration page of the standalong ui.
 *
 * @author Matthias Bach
 * @since 0.1
 * @version 0.2
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
require_once( 'contrib/timer.php' );
$scriptTimer = new Timer();

// check what we shall do
$action = $_POST['action'];

require_once( 'contrib/smarty/Smarty.class.php' );
$smarty = new Smarty();

$engineTimer = new Timer();
require_once( 'inc/engine.php' );

switch ( $action ) {
	case 'drop':
		// drop the shared storage
		$OLDCONFIG = getConfig();
		if( ! is_true( $OLDCONFIG['disableConfigUI'] ) ) {
			$smarty->assign( 'error', 'You are not allowed to drop the data from the shared storage.' );
		} else {
			if( dropData() ) {
				$smarty->assign( 'performedAction', 'Droped the data from shared storage.' );
			} else {
				$smarty->assign( 'error', getError() );
			};
		}
		
		break;
		
	case 'reload':
		// reload configuration from base-config-file
		include( $BASECONFIG_FILE );
		
		if( ! isset ( $BASECONFIG ) ) {
			$smarty->assign( 'error', 'The configuration file did not contain a variable called $BASECONFIG.' );
			break;
		}

		if( setConfig( $BASECONFIG ) ) {
			$smarty->assign( 'performedAction', 'Reloaded configuration from base configuration file.' );
		} else {
			$smarty->assign( 'error', getError() );
		};
		
		break;
		
	case 'store':
		// check whether config was given, if yes then store
		$BASECONFIG = $_POST['BASECONFIG'];
		if( ! isset( $BASECONFIG ) ) {
			$smarty->assign( 'error', 'No configuration given in request. There is probably some error in the previous page.' );
		} else {
			$OLDCONFIG = getConfig();
			if( (bool) $OLDCONFIG['disableConfigUI'] ) {
				$smarty->assign( 'error', 'You are not allowed to update the configuration.' );
			} else {
				if( setConfig( $BASECONFIG ) ) {
					$smarty->assign( 'performedAction', 'Stored new configuration' );
				} else {
					$smarty->assign( 'error', getError() );
				};
			}
		}
		break;
		
	default:
		if( isset( $action ) ) // only print error if there is actually an action set
			$smarty->assign( 'error', "Unkown action $action requested.");
}

// get configuration and set for display
$CONFIG = getConfig();
if( $CONFIG == NULL ) {
	// make sure we don't loose previous errors
	$smarty->append( 'error', getError() );
} else {
	$smarty->assign( 'config', $CONFIG );
}

$smarty->assign( 'stylesheet', resolveStylesheet( $CONFIG ) );

// get plugin-data
$dataPlugins = getDataProviderPlugins();
if( $dataPlugins == NULL ) {
	$smarty->append( 'error', getError() );
} else {
	$smarty->assign( 'dataPlugins', $dataPlugins );
}

$plotPlugins = getPlotPlugins();
if( $plotPlugins == NULL ) {
	$smarty->append( 'error', getError() );
} else {
	$smarty->assign( 'plotPlugins', $plotPlugins );
}

// get list of stylesheets
$stylesheets = getStylesheets();
if( $stylesheets == NULL ) {
	$smarty->append( 'error', getError() );
} else {
	$smarty->assign( 'stylesheets', $stylesheets );
}

// make sure the form calls the right script
$smarty->assign( 'configScript', $_SERVER['PHP_SELF'] );

// help the template figure out boolean values
$smarty->register_function( 'is_true', 'is_true' );

$smarty->assign( 'scriptTimer', $scriptTimer );
// render the data
$renderTimer = new Timer();
$smarty->assign( 'renderTimer', $renderTimer );
$smarty->display( 'config.tpl' );

?>
