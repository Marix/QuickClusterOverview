<?php
/**
 * A simple utility class for measuring time intervalls. (e.g. Runtimes)
 *
 * @author Matthias Bach <marix@marix.org>
 * @version 0.1
 * @since 0.1
 */

class Timer {
	var $start;
	var $end;

        /**
         * Creates a new timer. Mesurement will start in this moment
         */
	function Timer() {
		$this->start = microtime();
	}

        /**
         * Stops the time measurment and therefore specifies the intervall that will
         * be given by getTime().
         * Can be called repeatedly to increase the intervall, giving somewhat
         * of a split time functionality
         */
	function stop() {
		$this->end = microtime();
	}

    /**
	 * Returns the Timeintervall between the creation of the timer and the last
	 * call to stop() in seconds with 2 digits precision. If stop() has not been
	 * called it will be called here implicitly.
	 *
	 * @return float A time-intervall in seconds
	 */
	function getTime() {
		if( !isset( $this->end ) )
			$this->stop();

		list($start_msec, $start_sec ) = explode( " ", $this->start );
		list($end_msec, $end_sec ) = explode( " ", $this->end );

		$start = (float) $start_msec + (float) $start_sec;
		$end = (float) $end_msec + (float) $end_sec;

        // standard php-presicion will only be exact to 0.01 s.
        // increase php-precision if you want better times.
		return (float) round( ( (float) $end - (float) $start ),  2  );
	}
}	 
?>
