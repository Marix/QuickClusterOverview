<?php
/**
 * A gnuplot plot plugin
 *
 * This is an implementation of the plot plugin interface that will plot the
 * data using gnuplot.
 * 
 * @author Matthias Bach <marix (at) marix (dot) org>
 * @since 0.1
 * @version 0.2
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
require( './contrib/PHP_GNUPlot.php' );
 
/*
 * the methods implementing the interface can be found
 * all the way on the bottom of this file
 */

/*
 * Plugin internal variables that should not be used from the outside.
 */
 
/**
 * Holds the error string in case there was an error during plotting.
 */
global $gnuplot_error;

/*
 * Public functions implementing the interface 
 */
 
function gnuplot_getName() {
	return 'Gnuplot';
}

function gnuplot_getDescription() {
	return 'Plots the data using the classic gnuplot program';
}

function gnuplot_getVersion() {
	return 0.2;
}

function gnuplot_getAuthor() {
	return 'Matthias Bach <marix (at) marix (dot) org';
}

function gnuplot_getInterfaceVersion() {
	return 0.1;
}

function gnuplot_plot( $CLUSTER_DATA, $CONFIG ) {
	global $gnuplot_error;
	
	// figure out some meta-info
	$currentSet = (int) $CLUSTER_DATA['currentSet'];
	$basePath = $CONFIG['graphBasePath'];
	
	// initialize gnuplot
	$gnuplot = new GNUPlot();
	
	$gnuplot->set('boxwidth 1');
	
	// if fill is set to true in the configuration
	if( is_true( $CONFIG['ALLGRAPHS']['fill'] ) ) {
		$gnuplot->set( 'style fill solid' );
	}
	
	foreach( $CONFIG['GRAPHS'] as $GRAPH ) {
		// set meta-inf
		$gnuplot->setTitle( $GRAPH['title'] );
		$file = $GRAPH['file'];
		if( !isset( $file ) ) {
			$gnuplot_error = 'No file given for plot of ' . $GRAPH['title'];
			return FALSE;
		}
		$gnuplot->setOutput( $basePath . $file );
		
		// set size ( maybe fallback )
		$xsize = 1; $ysize = 1; // default
		if( isset( $GRAPH['SIZE']['x'] ) && $GRAPH['SIZE']['x'] != '') // check whether graph specific x-size is given ( empty string means not given
			$xsize = $GRAPH['SIZE']['x'];
		else if ( isset( $CONFIG['ALLGRAPHS']['SIZE']['x'] ) && $CONFIG['ALLGRAPHS']['SIZE']['x'] != '') // if not, check for global
			$xsize = $CONFIG['ALLGRAPHS']['SIZE']['x'];
		if( isset( $GRAPH['SIZE']['y'] ) && $GRAPH['SIZE']['y'] != '') // check whether graph specific y-size is given
			$ysize = $GRAPH['SIZE']['y'];
		else if ( isset( $CONFIG['ALLGRAPHS']['SIZE']['y'] ) && $CONFIG['ALLGRAPHS']['SIZE']['y'] != '') // if not, check for global
			$ysize = $CONFIG['ALLGRAPHS']['SIZE']['y'];
			
		$gnuplot->setSize( $xsize, $ysize );
		
		
		// check whether to set y-range
		$ymin = '*'; $ymax = '*'; // * sets autoscale (default)
		if( isset( $GRAPH['YRANGE']['min'] ) ) // check whether graph specific yrange minimum is given
			$ymin = $GRAPH['YRANGE']['min'];
		else if ( isset( $CONFIG['ALLGRAPHS']['YRANGE']['min'] ) ) // if not check for global
			$ymin =  $CONFIG['ALLGRAPHS']['YRANGE']['min'];
		if( isset( $GRAPH['YRANGE']['max'] ) ) // the same in green
			$ymax = $GRAPH['YRANGE']['max'];
		else if ( isset( $CONFIG['ALLGRAPHS']['YRANGE']['max'] ) )
			$ymax =  $CONFIG['ALLGRAPHS']['YRANGE']['max'];
		// check whether auto has been selected
		if( $ymin == 'auto' )
			$ymin = '*';
		if( $ymax == 'auto' )
			$ymax = '*';
		
		$gnuplot->setRange( 'y', $ymin, $ymax );
		
		// check what to plot 
		$metricID = $GRAPH['metric'];
		$range = 0;
		
		// maybe this is a mutliplot
		$METRICS = explode( '|', $metricID );
		
		$data = array();
		for( $i = 0; $i < sizeof( $METRICS ); $i++ ) {
			$metric = $METRICS[$i];	
				
			// build data structure and plot
			$metricData = new PGData( $CLUSTER_DATA['DATA_SETS'][$currentSet][$metric]['name'] );
		
			// currently only current set is plotted
			if( !is_array( $CLUSTER_DATA['DATA_SETS'][$currentSet][$metric]['NODES'] ) || sizeof ($CLUSTER_DATA['DATA_SETS'][$currentSet][$metric]['NODES']) == 0 ) {
				$gnuplot_error = "No data given for metric $metric.";
				return FALSE;
			}
			
			// plot the nodes over some generic counter, not over their id (gnuplot can't handle)
			$counter = 0;
			foreach( $CLUSTER_DATA['DATA_SETS'][$currentSet][$metric]['NODES'] as $NODE ) {
				$summedValue = $NODE['value'];
				for( $iSum = $i + 1; $iSum < sizeof( $METRICS ); $iSum++ ) 
					$summedValue += $CLUSTER_DATA['DATA_SETS'][$currentSet][$METRICS[$iSum]]['NODES'][$counter]['value'];
				
				$metricData->addDataEntry( array( ($counter++) + .5, $summedValue ) );
			}
			
			$data[] = $metricData;
			
			// check how big this has to be
			if( $counter > $range )
				$range = $counter;
		}
		
		// adjust x-range
		$gnuplot->setRange( 'x', 0, $range );
		
		// check whether this was a mutliplot after all
		if( sizeof( $data ) == 1 ) {
			$data = $data[0];
		}
		
		// plot the data as 2d without any extra fancy
		$gnuplot->plotData( $data, 'boxes', '1:2' );
		
	}
	
	// we are done, close gnuplot
	$gnuplot->close();
	
	return TRUE;
}

function gnuplot_getError() {
	global $gnuplot_error;
	return $gnuplot_error;
}

?>
