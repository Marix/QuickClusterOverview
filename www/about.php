<?php
/**
 * About page
 * 
 * This is the about page of the standalong ui. Just some general information
 * is displayed.
 * 
 * @author Matthias Bach
 * @since 0.1
 * @version 0.2
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
require_once( 'contrib/timer.php' );
$scriptTimer = new Timer();

require_once( 'inc/engine.php' );


// get configuration and set for display
$CONFIG = getConfig();
if( $CONFIG == NULL ) {
	// make sure we don't loose previous errors
	$error = getError();
}
$stylesheet = resolveStylesheet( $CONFIG );

$dataPlugins = getDataProviderPlugins();
if( $dataPlugins == NULL ) {
	$dataError = getError();
}

$plotPlugins = getPlotPlugins();
if( $plotPlugins == NULL ) {
	$plotError = getError();
}

// prepare the data
require_once( 'contrib/smarty/Smarty.class.php' );
$smarty = new Smarty();
if( $dataPlugins == NULL ) {
	$smarty->assign( 'dataError', $dataError );
} else {
	$smarty->assign( 'dataPlugins', $dataPlugins );
}
if( $plotPlugins == NULL ) {
	$smarty->assign( 'plotError', $plotError );
} else {
	$smarty->assign( 'plotPlugins', $plotPlugins );
}

if( isset( $stylesheet ) ) {
	$smarty->assign( 'stylesheet', $stylesheet);
} else {
	$smarty->assign( 'error', $error );
}

$smarty->assign( 'scriptTimer', $scriptTimer );
// render the data
$renderTimer = new Timer();
$smarty->assign( 'renderTimer', $renderTimer );
$smarty->display( 'about.tpl' );

?>
