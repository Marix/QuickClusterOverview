<?php
/**
 * Json-Graph provider
 * 
 * This page refreshes the graphs ans if successfull returns an array of graphs
 * in JSON-notation (see http://json.org). The first element in the array is
 * reserverd for the array variable.
 *
 * The returned array will contain the error at index 0. If no error occured the
 * error will be set to an empty string and a list of graphs can be found at index
 * 1, a list of nodes at index 2.
 * 
 * @author Matthias Bach
 * @since 0.1
 * @version 0.1
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

require_once( 'contrib/JSON.php' ); 
require_once( 'inc/engine.php' );

// pull data from cluster/cache
$data = getData();

$result = array(); 
if( $data == NULL ) {
	$result[0] = getError();
} else {
	$result[0] = ''; // we should always fill the first element somehow
	
	// add list of graphs to result
	$graphs = array();
	foreach( $data['CONFIG']['GRAPHS'] as $GRAPH ) {
		$graphs[] = $data['CONFIG']['graphBasePath'] . $GRAPH['file'];
	}
	$result[1] =  $graphs;
	
	// add list of nodes to results (exemplary for first metric)
	$nodes = array();
	$currentSet = (int) $CLUSTER_DATA['currentSet'];
	foreach( $data['CLUSTER_DATA']['DATA_SETS'][ $currentSet ] as $key => $metric ) {
		if( $key == 'timestamp' )
			continue; // we don't want to loop over the timestamp ;)
			
		foreach( $metric['NODES'] as $NODE ) {
			if( isset( $NODE['NAME'] ) ) {
				$nodes[] = $NODE['NAME'];
			} else {
				$nodes[] = $NODE['id'];
			}
		}
		
		// other metrics shouldn't provider more info
		break;
	}
	$result[2] = $nodes;
}

// render the data
$json = new Services_JSON();
$output = $json->encode( $result );

print( $output );
// done
?>
