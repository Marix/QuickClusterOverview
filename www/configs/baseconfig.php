<?php
/**
* Basic configuration file
*
* This file contains the basic configuration that will be available even after
* the shared data has been dumped.
*
* Be aware that this file can be overwritten if the data is changed via the
* application user interface.
*
* If you modify this file you have to make sure it's reloaded by the application,
* which it usually isn't during execution. 
*
* @author Matthias Bach
* @since 0.1
* @version 0.1
*/

// bevaviour configuration

$BASECONFIG['intervall'] = 5; // update intervall
$BASECONFIG['maxHistory'] = 1; // how many datasets shall be held at the same time
$BASECONFIG['showNodeList'] = 'default'; // Whether to show the list of nodes on the frontpage. Possible values are show, optional and none. If not set it defaults to optional.
//$BASECONFIG['stylesheet'] = 'default.css'; // The stylesheet to use. If omitted hardcoded default will be used.
$BASECONFIG['disableConfigUI'] = 'false'; // If set to any value other than false, an emtpy string or zero (or not at all) it will not be possible to edit the configuration via the ui. All configuration changes will have to be made via the baseconfig file and loaded into the application using the cmdcli in the testing directory.


// plot configuration

$BASECONFIG['plotPlugin'] = 'gnuplot'; // which plugin to use for plotting
$BASECONFIG['graphBasePath'] = './graphs/'; // where to put the plots (make sure to include the traling slash). keep in mind that this directory has to be writeable by the webserver.


// some default values that can be overriden for single plots

$BASECONFIG['ALLGRAPHS']['YRANGE']['min'] = '0'; // The minimum value for the y-range. Can be set to auto for autoscaling
$BASECONFIG['ALLGRAPHS']['YRANGE']['max'] = 'auto'; // The maximum value for the y-range. Can be set to auto for autoscaling
//$BASECONFIG['ALLGRAPHS']['SIZE']['x'] = '1'; // The size in x-direction that shall be used for all graphs that do not have an own value given
$BASECONFIG['ALLGRAPHS']['SIZE']['y'] = '.5'; // The size in y-direction that shall be used for all graphs that do not have an own value given
$BASECONFIG['ALLGRAPHS']['fill'] = 'false'; // If set to any value other than false, an emtpy string or zero (or not at all) the graphs will be plotted in a filled style. This might not work with older version of gnuplot, so.


// definition of each plot that shall be made

$BASECONFIG['GRAPHS'][0]['title'] = 'System Load'; // The title to be used for the plot
$BASECONFIG['GRAPHS'][1]['title'] = 'CPU Usage'; // The title to be used for the plot
$BASECONFIG['GRAPHS'][0]['metric'] = 'load_one|load_five'; // The metric that will be used for this graph. You can specify multiple metrics seperated by a vertical bar like "cpu_nice|cpu_user|cpu_system". This will display the metrics in one graph on top of each other, with the first metric given being the uppermost.
$BASECONFIG['GRAPHS'][1]['metric'] = 'cpu_nice|cpu_user|cpu_system'; // The metric that will be used for this graph. You can specify multiple metrics seperated by a vertical bar like "cpu_nice|cpu_user|cpu_system". This will display the metrics in one graph on top of each other, with the first metric given being the uppermost.
$BASECONFIG['GRAPHS'][0]['file'] = 'load_one.png'; // The name of the file the plot will be stored in below the graphBasePath
$BASECONFIG['GRAPHS'][1]['file'] = 'cpu_system.png'; // The name of the file the plot will be stored in below the graphBasePath
$BASECONFIG['GRAPHS'][0]['YRANGE']['min'] = '0'; // The minimum value for the y-range. Can be set to auto for autoscaling. If not set global value will be used.
$BASECONFIG['GRAPHS'][1]['YRANGE']['min'] = '0'; // The minimum value for the y-range. Can be set to auto for autoscaling. If not set global value will be used.
//$BASECONFIG['GRAPHS'][0]['YRANGE']['max'] = 'auto'; // The maximum value for the y-range. Can be set to auto for autoscaling. If not set global value will be used.
$BASECONFIG['GRAPHS'][1]['YRANGE']['max'] = '100'; // The maximum value for the y-range. Can be set to auto for autoscaling. If not set global value will be used.
//$BASECONFIG['GRAPHS'][0]['SIZE']['x'] = '1'; // The size in x-direction that shall be used for the graph. If not set global value will be used.
//$BASECONFIG['GRAPHS'][1]['SIZE']['x'] = '1'; // The size in x-direction that shall be used for the graph. If not set global value will be used.
//$BASECONFIG['GRAPHS'][0]['SIZE']['y'] = '.5'; // The size in y-direction that shall be used for the graph. If not set global value will be used.
//$BASECONFIG['GRAPHS'][1]['SIZE']['y'] = '.5'; // The size in y-direction that shall be used for the graph. If not set global value will be used.


// definition of where to pull the data from (and which)

$BASECONFIG['DATASOURCE']['name'] = 'ganglia'; // name of the plugin providing the data
$BASECONFIG['DATASOURCE']['PROVIDER'][0]['name'] = 'localhost'; // The name of the computer the data will be pulled from
$BASECONFIG['DATASOURCE']['PROVIDER'][0]['port'] = '8649'; // The port on the remote computer to connect for. Not required by all plugins


?>