<?php
/**
* Basic configuration file
*
* This file contains the basic configuration that will be available even after
* the shared data has been dumped.
*
* Be aware that this file can be overwritten if the data is changed via the
* application user interface.
*
* If you modify this file you have to make sure it's reloaded by the application,
* which it usually isn't during execution. 
*
* @author Matthias Bach
* @since 0.1
* @version 0.1
*/

//bevaviour configuration
$BASECONFIG['intervall'] = 5; // update intervall
$BASECONFIG['maxHistory'] = 1; // how many datasets shall be held at the same time

// plot configuration
$BASECONFIG['plotPlugin'] = 'gnuplot'; // which plugin to use for plotting
$BASECONFIG['graphBasePath'] = './graphs/'; // where to put the plots (make sure to include the traling slash)
// keep in mind that this directory has to be writeable by the webserver

// definition of each plot that shall be made
$BASECONFIG['GRAPHS'][0]['title'] = 'Load average';
$BASECONFIG['GRAPHS'][0]['metric'] = 'LOADAVG';
$BASECONFIG['GRAPHS'][0]['file'] = 'load_avg.png';

$BASECONFIG['GRAPHS'][1]['title'] = 'System CPU Usage in %';
$BASECONFIG['GRAPHS'][1]['metric'] = 'CPUUTILPERCSYSTEM';
$BASECONFIG['GRAPHS'][1]['file'] = 'cpu_system.png';

$BASECONFIG['GRAPHS'][2]['title'] = 'User CPU Usage in %';
$BASECONFIG['GRAPHS'][2]['metric'] = 'CPUUTILPERCUSER';
$BASECONFIG['GRAPHS'][2]['file'] = 'cpu_user.png';

$BASECONFIG['GRAPHS'][3]['title'] = 'Average interupts per second';
$BASECONFIG['GRAPHS'][3]['metric'] = 'AVRINTERRUPTSPERSEC';
$BASECONFIG['GRAPHS'][3]['file'] = 'interupts.png';

// definition of where to pull the data from (and which)
$BASECONFIG['DATASOURCE']['name'] = 'lemon_lrf'; // name of the plugin providing the data
$BASECONFIG['DATASOURCE']['PROVIDER'][0]['name'] = '/var/www/html/lrf/';
$BASECONFIG['DATASOURCE']['PROVIDER'][0]['port'] = '';
// if the plugin doesn need a list of clients. otherwise we would have to define them here
//$BASECONFIG['DATASOURCE']['PROVIDER'][0]['CLIENTS'] = 'foo bla 42';
?>