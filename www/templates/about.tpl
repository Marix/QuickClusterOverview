<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title>QCO - About</title>
  <meta name="GENERATOR" content="Quanta Plus" />
  <meta name="AUTHOR" content="Matthias Bach" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="{$stylesheet}" />
</head>
<body>

{include file='header.tpl'}

<h2>About</h2>

<p>Quick Cluster Overview is a utility UI to squeeze every cluster node on one screen for the most important metrics. Currently it works with <a href="http://ganglia.info/">ganglia</a> or <a href="http://lemon.web.cern.ch">lemon</a> as a backend for cluster data retrieval. However it can be extended to work with other monitoring systems by adding additional data provider plugins.</p>

<p>Version: 0.2</p>

<h2>Installed data provider plugins</h2>

{if isset( $dataError ) }
	<p class="error">{$dataError}</p>
{else}
	{foreach from=$dataPlugins item=plugin}
		<table class="plugin">
			<tr class="plugin"><td class="pluginlabel">Name:</td><td class="plugin">{$plugin.name}</td></tr>
			<tr class="plugin"><td class="pluginlabel">Version:</td><td class="plugin">{$plugin.version}</td></tr>
			<tr class="plugin"><td class="pluginlabel">Description:</td><td class="plugin">{$plugin.description}</td></tr>
		</table>
	{foreachelse}
		<p class="warning">No data provider plugins installed.</p>
	{/foreach}
{/if}

<h2>Installed plot plugins</h2>

{if isset( $plotError ) }
	<p class="error">{$plotError}</p>
{else}
	{foreach from=$plotPlugins item=plugin}
		<table class="plugin">
			<tr class="plugin"><td class="pluginlabel">Name:</td><td class="plugin">{$plugin.name}</td></tr>
			<tr class="plugin"><td class="pluginlabel">Version:</td><td class="plugin">{$plugin.version}</td></tr>
			<tr class="plugin"><td class="pluginlabel">Description:</td><td class="plugin">{$plugin.description}</td></tr>
		</table>
	{foreachelse}
		<p class="warning">No data provider plugins installed.</p>
	{/foreach}
{/if}

<h2>Used third party stuff</h2>

<p>This program was written using <a href="http://www.php.net">PHP</a> and utilizes the <a href="http://smarty.php.net">Smarty template engine</a> for output. For plotting it uses <a href="http://gnuplot.info">gnuplot</a> which is accessed via <a href="http://php-gnuplot.sourceforge.net/">PHP-GNUPlot</a>.</p>

{include file='footer.tpl'}

</body>
</html>
