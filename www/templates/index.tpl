<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title>QCO - Main Page</title>
  <meta name="GENERATOR" content="Quanta Plus" />
  <meta name="AUTHOR" content="Matthias Bach" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="refresh" content="{$updateIntervall}" />
  <link rel="stylesheet" type="text/css" href="{$stylesheet}" />
</head>
<body>

{include file='header.tpl'}

{if isset( $error ) }
	{* An error occured -> display instead of graphs *}
	<p class='error'>{$error}</p>
{else}
	{* no error *}
	<div id="graphs">
	{foreach from=$graphs item=graph}
		<img class="graph" src="{$graph}" />
	{foreachelse}
		<p class='error'>No graphs have been defined. Please go to the configuration page to set up some graphs.</p>
	{/foreach}
	</div>
	
	{if $showNodeList == 'show' }
		<div class="nodelist">
			These are the nodes on this cluster:
			<ol class="nodelist">
				{foreach from=$nodes item=node}
					<li class="nodelist">{$node}</li>
				{/foreach}
			</ol>
			<br class="nodelist" />	
		</div>
	{elseif $showNodeList == 'optional' }
		<p class="nodelist"><a href="index.php?showNodeList=show">Show list of nodes</a></p>
	{/if}

{/if}

{include file='footer.tpl'}

</body>
</html>
