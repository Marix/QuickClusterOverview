<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
  <title>QCO - Configuration</title>
  <meta name="GENERATOR" content="Quanta Plus" />
  <meta name="AUTHOR" content="Matthias Bach" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" type="text/css" href="{$stylesheet}" />
</head>
<body>

{include file='header.tpl'}

{if is_true( $config.disableConfigUI ) }
	<p class="warning">The configuration UI has been disabled in the configuration file. To change the configuration you will have to edit
	the configuration file. The only thing you can currently do in this UI is reload the configuration.</p>
{/if}

{* print any errors that occured. be aware that there is the possibility multiple errors occured *}
{if isset( $error ) }
	{if is_array( $error ) }
		{foreach from=$error item=item}
			<p class="error">{$item}</p>
		{/foreach}
	{else}
		<p class="error">{$error}</p>
	{/if}
{/if}

{* notify user of permormed actions (except retrieving config, that is always done) *}
{if isset( $performedAction ) }
	<p class="warning">{$performedAction}</p>
{/if}

{* finally display the configuration ui *}

<form class="config" action="{$configScript}" method="post">
	<input class="config" type="hidden" name="action" value="store" />
	
	<fieldset class="config">
		<legend class="config" >Basic Configuration</legend>
		
		<label class="config" for="intervall">Update intervall</label>
		<input class="config"  type="input" id="intervall" name="BASECONFIG[intervall]" value="{$config.intervall}" />
		<p class="configDescription">The intervall in seconds after which the data and diagramms are refreshed.</p>
		
		<label class="config" for="maxHistory">Number of historic datasets</label>
		<input class="config"  type="input" id="maxHistory" name="BASECONFIG[maxHistory]" value="{$config.maxHistory}" />
		<p class="configDescription">The maximum number of datasets that are held in memory at one time. Currently this does not have any effect so it's safe to leave this set to 1.</p>
		
		<label class="config" for="showNodeList">Show list of nodes</label>
		<select class="config"  type="input" id="showNodeList" name="BASECONFIG[showNodeList]">
			<option value="show" {if $config.showNodeList == 'show' } selected="selected" {/if}>Always</option>
			<option value="optional" {if $config.showNodeList != 'show' && $config.showNodeList != 'none' } selected="selected" {/if}>Optional</option>
			<option value="none" {if $config.showNodeList == 'none' } selected="selected" {/if}>Never</option>
		</select>
		<p class="configDescription">Whether to show the list of nodes on the frontpage. If not set it defaults to optional.</p>
	
		<label class="config" for="stylesheet">Stylesheet</label>
		<select class="config"  id="stylesheet" name="BASECONFIG[stylesheet]">
			{foreach from=$stylesheets item=stylesheet}
				<option value="{$stylesheet}" {if $stylesheet == $config.stylesheet} selected="selected" {/if}>{$stylesheet}</option>
			{/foreach}
		</select>
		<p class="configDescription">The stylesheet to use.</p>
	
	</fieldset>
	
	<fieldset class="config">
		<legend class="config" >Plot Configuration</legend>
		
		<label class="config" for="plotPlugin">Plot Plugin</label>
		<select class="config"  id="plotPlugin" name="BASECONFIG[plotPlugin]">
			{foreach from=$plotPlugins item=plugin}
				<option value="{$plugin.id}" {if $plugin.id == $config.plotPlugin} selected="selected" {/if}>{$plugin.name}</option>
			{/foreach}
		</select>
		<p class="configDescription">The selected plugin will be used to render the graphs.</p>
		
		<label class="config" for="graphBasePath">Graph base path</label>
		<input class="config"  type="input" id="graphBasePath" name="BASECONFIG[graphBasePath]" value="{$config.graphBasePath}" />
		<p class="configDescription">The directory the graphs will be stored in. This directory has to be writeable by the webserver.</p>
		
		<label class="config" for="allgraphs_yrange_min">Minimum y-range value</label>
		<input class="config"  type="input" id="allgraphs_yrange_min" name="BASECONFIG[ALLGRAPHS][YRANGE][min]" value="{$config.ALLGRAPHS.YRANGE.min}" />
		<p class="configDescription">The minimum value of the y-range. This value can be overriden for individual plots. Set this value to "auto" for autoscaling.</p>
	
		<label class="config" for="allgraphs_yrange_max">Maximum y-range value</label>
		<input class="config"  type="input" id="allgraphs_yrange_max" name="BASECONFIG[ALLGRAPHS][YRANGE][max]" value="{$config.ALLGRAPHS.YRANGE.max}" />
		<p class="configDescription">The maximum value of the y-range. This value can be overriden for individual plots. Set this value to "auto" for autoscaling.</p>
	
		<label class="config" for="allgraphs_size_x">Default graph size in x-direction</label>
		<input class="config"  type="input" id="allgraphs_size_x" name="BASECONFIG[ALLGRAPHS][SIZE][x]" value="{$config.ALLGRAPHS.SIZE.x}" />
		<p class="configDescription">The default size of the graphs in x-direction. This value can be overriden for individual plots. Leave empty for default.</p>

		<label class="config" for="allgraphs_size_y">Default graph size in y-direction</label>
		<input class="config"  type="input" id="allgraphs_size_y" name="BASECONFIG[ALLGRAPHS][SIZE][y]" value="{$config.ALLGRAPHS.SIZE.y}" />
		<p class="configDescription">The default size of the graphs in y-direction. This value can be overriden for individual plots. Leave empty for default.</p>
	
		<label class="config" for="allgraphs_fill">Fill Boxes</label>
		<select class="config"  type="input" id="allgraphs_fill" name="BASECONFIG[ALLGRAPHS][fill]">
			<option value="true" {if is_true($config.ALLGRAPHS.fill) } selected="selected" {/if}>On</option>
			<option value="false" {if !is_true( $config.ALLGRAPHS.fill) } selected="selected" {/if}>Off</option>
		</select>
		<p class="configDescription">If set the graphs will be plotted in a filled style. This will not work with version of gnuplot before 4.0.</p>
</fieldset>
	
	{* loop over graphs using section wich is the more mighty brother of foreach *}
	{section name=graphsSection loop=$config.GRAPHS}
		{assign var=id value=$smarty.section.graphsSection.index}
		{assign var=graph value=$config.GRAPHS[graphsSection]}
		<fieldset class="config">
			<legend class="config" >Graph {$id}</legend>
			
			<label class="config" for="graph_{$id}_title">Title</label>
			<input class="config"  type="input" id="graph_{$id}_title" name="BASECONFIG[GRAPHS][{$id}][title]" value="{$graph.title}" />
			<p class="configDescription">The title that will be used for this graph.</p>
			
			<label class="config" for="graph_{$id}_metric">Metric</label>
			<input class="config"  type="input" id="graph_{$id}_metric" name="BASECONFIG[GRAPHS][{$id}][metric]" value="{$graph.metric}" />
			<p class="configDescription">The metric that will be used for this graph. You can specify multiple metrics seperated by a vertical bar like "cpu_nice|cpu_user|cpu_system". This will display the metrics in one graph on top of each other, with the first metric given being the uppermost.</p>
			
			<label class="config" for="graph_{$id}_file">Filename</label>
			<input class="config"  type="input" id="graph_{$id}_file" name="BASECONFIG[GRAPHS][{$id}][file]" value="{$graph.file}" />
			<p class="configDescription">The filename this graph will be stored as.</p>
			
			<label class="config" for="graph_{$id}_yrange_max">Maximum y-range value</label>
			<input class="config"  type="input" id="graph_{$id}_yrange_max" name="BASECONFIG[GRAPHS][{$id}][YRANGE][max]" value="{$graph.YRANGE.max}" />
			<p class="configDescription">The maximum value of the y-range. Set this value to "auto" for autoscaling. Leave empty for default.</p>
			
			<label class="config" for="graph_{$id}_yrange_min">Minimum y-range value</label>
			<input class="config"  type="input" id="graph_{$id}_yrange_min" name="BASECONFIG[GRAPHS][{$id}][YRANGE][min]" value="{$graph.YRANGE.min}" />
			<p class="configDescription">The minimum value of the y-range. Set this value to "auto" for autoscaling. Leave empty for default.</p>
		
			<label class="config" for="graph_{$id}_size_x">Size in x-direction</label>
			<input class="config"  type="input" id="graph_{$id}_size_x" name="BASECONFIG[GRAPHS][{$id}][SIZE][x]" value="{$graph.SIZE.x}" />
			<p class="configDescription">The size in x-direction to use for this graph. Leave empty for default.</p>

			<label class="config" for="graph_{$id}_size_x">Size in y-direction</label>
			<input class="config"  type="input" id="graph_{$id}_size_y" name="BASECONFIG[GRAPHS][{$id}][SIZE][y]" value="{$graph.SIZE.y}" />
			<p class="configDescription">The size in y-direction to use for this graph. Leave empty for default.</p>
		</fieldset>
	{/section}
	
	<fieldset class="config">
		<legend class="config" >Datasource</legend>

		<label class="config" for="datasource_plugin">Data Plugin</label>
		<select class="config"  id="datasource_plugin" name="BASECONFIG[DATASOURCE][name]">
			{foreach from=$dataPlugins item=plugin}
				<option value="{$plugin.id}" {if $plugin.id == $config.DATASOURCE.name} selected="selected" {/if} >{$plugin.name}</option>
			{/foreach}
		</select>
		<p class="configDescription">The selected plugin will be used to retrieve the data from the cluster.</p>
	
		<label class="config" for="datasource_provider_0_name">Host</label>
		<input class="config"  type="input" id="datasource_provider_0_name" name="BASECONFIG[DATASOURCE][PROVIDER][0][name]" value="{$config.DATASOURCE.PROVIDER.0.name}" />
		<p class="configDescription">The name of the computer to connect to for retrieving the cluster data. For the lemon LRF plugin this is not a computer, but a path to the lemon web ui.</p>
		
		<label class="config" for="datasource_provider_0_port">Port</label>
		<input class="config"  type="input" id="datasource_provider_0_port" name="BASECONFIG[DATASOURCE][PROVIDER][0][port]" value="{$config.DATASOURCE.PROVIDER.0.port}" />
		<p class="configDescription">The port to connect to for retrieving the cluster data.</p>
	
	</fieldset>
	
	
	<input class="config" type="submit" value="Store Configuration" {if is_true( $config.disableConfigUI )} disabled="disabled" {/if}/><input class="config" type="reset" value="Reset"/>
</form>

<form class="config" action="{$configScript}" method="post">
	<input class="config" type="hidden" name="action" value="reload" />
	<p class="configDescription">You can reload the data from the configuration file. This is required if you edited the configuration file by hand, as the configuration is during runtime the configuration is stored in shared memory.</p>
	<input type="submit" value="Reload Configuration" />
</form>

<form class="config" action="{$configScript}" method="post">
	<input class="config" type="hidden" name="action" value="drop" />
	<p class="configDescription">You can drop the data from the shared storage. This has the effect of reseting the application. All data will be reloaded the next time the page is viewed.</p>
	<p class="warning">Be aware that droping the data will force a reload and redrawing of data from the cluster. Therefore frequent droping may impact the applications performance.</p>
	<input type="submit" value="Drop shared data" {if is_true( $config.disableConfigUI )} disabled="disabled" {/if} />
</form>

{include file='footer.tpl'}

</body>
</html>
