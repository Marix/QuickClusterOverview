<?php
/**
 * A data retrieval plugin for lemon that uses the simplified
 * message repository API. Sadly that support for this API will
 * be discontinued at the end of september 2006, however using
 * that API has the big advantage of being able to connect to
 * a remote pc.
 *
 * @author Matthias Bach <marix (at) marix (dot) org>
 * @version 0.1
 * @since 0.1
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
/*
 * the methods implementing the interface can be found
 * all the way on the bottom of this file
 */

/*
 * Plugin internal variables that should not be used from the outside.
 */

global $LEMON_MRS_LIB_PATH; 
$LEMON_MRS_LIB_PATH = '/usr/lib/FmonMRs_php.so'; // TODO this should be retrieved from configuration
 
/**
 * Holds the error string in case there was an error during plotting.
 */
global $lemon_mrs_error;

/*
 * public methods that will be called during interface usage
 */
function lemon_mrs_getName() {
	return 'Lemon MRS';
}

function lemon_mrs_getDescription() {
	return 'A plugin that retrievs the system status from Lemon using the old simplified repository API. This plugin requires the simplified repsitory api (packge mrs) to be installed on the system in order to work.';
}

function lemon_mrs_getVersion() {
	return 0.1;
}

function lemon_mrs_getAuthor() {
	return 'Matthias Bach <marix (at) marix (dot) org>';
}

function lemon_mrs_getInterfaceVersion() {
	return 0.1;
}

function lemon_mrs_getData( &$metrics, $config ) {
	global $lemon_mrs_error;
	global $LEMON_MRS_LIB_PATH;
	
	// get a list of all clients to query for (assume space-seperated)
	$clients = $config[ 'DATASOURCE' ]['CLIENTS'];
	
	// build a list of all metrics to query for
	$metrics_string;
	foreach( $metrics as $metricid => $metric ) {
		$metrics_string .= $metricid;
		$metrics_string .= '';
	}
	
	// prepare environment before initialization
	// first extract important config data
	$sources = $config[ 'DATASOURCE' ][ 'PROVIDER' ];
	if( sizeof( $sources ) == 0 ) {
		$lemon_mrs_error = 'No sources given';
		return FALSE;
	}
	
	// TODO loop over multiple sources if available
	// for now get first and be happy
	$host = $sources[0]['name'];
	$port = $sources[0]['port'];
	
	if( strncmp( 'http://', $host, 7 ) ) { // if we have a url set the url
		putenv( 'MR_SERVER_URL=' . $host . ':' . $port );
	} else {
		// otherwise this can only be a path to a spooldir, in that case the port does not have a meaning
		putenv( 'MR_SPOOLDIR=' . $host );
	}
	
	// check whether lemon simplified api is loaded (proper way is to load via php.ini)
	if( ! extension_loaded( 'FmonMRs_php' ) ) {
		// TODO this should generate a warning as dl is deprecated, slow and php.ini the proper way to do it
		if ( ! dl( $LEMON_MRS_LIB_PATH ) ) {
			$lemon_mrs_error = 'Failed to load simplified lemon repository API. Please load FmonMRs_php via the extension variable in php.ini or specify its location in datasources/lemon_mrs.php.';
			return FALSE;
		}
	}
	
	// now we should savely be able to use the extension
	// initialze
	if( MRs_open() == -1 ) {
		$lemon_mrs_error = MRs_getError();
		return FALSE; // bail out
	}
	// make query
	$handle = MRs_getLatestSamples( $clients , $metrics_string );
	if ($handle == -1) {
		// something went wrong
		$lemon_mrs_error = MRs_getError();
		return FALSE; // bail out
	}
	
	// store the data, we have to retrieve singe datasets
	while ( MRs_getNextSampleFromQuery( $handle, &$node, &$metric, &$timestamp, &$value) == 0) {
		$metrics[ $metric ]['NODES'][] = array( 'id' => $node, 'name' => $node, 'value' => $value );
	}
	
	// proberly cleanup
	
	if( MRs_close() == -1 ) {
		$lemon_mrs_error = MRs_getError();
		return FALSE; // bail out
	}
	
	// done
	return TRUE;
}

function lemon_mrs_getError() {
	global $lemon_mrs_error;
	return $lemon_mrs_error;
}

function lemon_mrs_requiresClientList() {
	return TRUE; // the simplified lemon api needs to send the clients to recieve data for
}

?>
