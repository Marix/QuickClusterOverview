<?php
/**
 * A data retrieval plugin for lemon utilising the lrs (lemon web ui).
 *
 * @author Matthias Bach <marix (at) marix (dot) org>
 * @version 0.1
 * @since 0.1
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
/*
 * the methods implementing the interface can be found
 * all the way on the bottom of this file
 */

/*
 * Plugin internal variables that should not be used from the outside.
 */
 
/**
 * Holds the error string in case there was an error during plotting.
 */
global $lemon_lrf_error;

/*
 * Plugin internal functions that should not be used from the outside.
 */
 


/*
 * public methods that will be called during interface usage
 */
function lemon_lrf_getName() {
	return 'Lemon LRF';
}

function lemon_lrf_getDescription() {
	return 'A plugin that retrievs the system status from Lemon using functionality of the native lemon web ui. This plugin requires the native web ui to be installed and configured on the system to work.';
}

function lemon_lrf_getVersion() {
	return 0.1;
}

function lemon_lrf_getAuthor() {
	return 'Matthias Bach <marix (at) marix (dot) org>';
}

function lemon_lrf_getInterfaceVersion() {
	return 0.1;
}

function lemon_lrf_getData( &$metrics, $config ) {
	global $lemon_lrf_error;
	
	// check where to find the lrf
	$lrf_root = $sources = $config[ 'DATASOURCE' ][ 'PROVIDER' ][0]['name']; // port and failover don't make sens in this context
	
	// switch to lrf-directory for lrf-internal includes to work (first back up old working dir to avoid wired behaviour of following modules)
	$own_wd = getcwd();
	if( $own_wd === FALSE ) {
		$lemon_lrf_error = 'Unable retrieve current working directory. Please check whether the webserver process executing this script has read access to its parent.';
		return FALSE;
	}
	
	if( ! chdir( $lrf_root ) ) {
		$lemon_lrf_error = 'Unable to switch to ' . $lrf_root . '. Please check whether this really is the working directory of the LRF and whether it is accessible by the webserver process that is executing this script.';
		return FALSE;
	}
	
	// the lemon lrf is not programmed really cleanly. therefore we have to reduce the log-level a little
	$own_logLevel = error_reporting( E_ERROR );

// retrieve lrf-configuration to know what datasource to use
	include( 'config.php' );
	
	// retrieve a repository access class from the mr-load utility
	include_once( 'mr_load.php' );
	$MR_i = get_MR_instance($BACKEND,$databases['lemon']);
	
	// iterate over all metrics and get new value
	foreach( $metrics as $metricKey => $metric ) {
		// make an attempt of getting the id of the metric
		if( isset( $metrics[ $metricKey ]['name'] ) ) {
			$metricid = $metrics[ $metricKey ]['name'];
			if( !isset( $metrics[ $metricKey ]['id'] ) ) {
				$metrics[ $metricKey ]['id'] = $metricid;
			}
		} else if( isset( $metrics[ $metricKey ]['id'] ) ) {
			$metricid = $metrics[ $metricKey ]['id'];
			if( !isset( $metrics[ $metricKey ]['name'] ) ) {
				$metrics[ $metricKey ]['name'] = $metricid;
			}
		} else {
			$metricid = $metricKey;
			$metrics[ $metricKey ]['id'] = $metricid;
			$metrics[ $metricKey ]['name'] = $metricid;
		}
		
		// retrieve data from lrf ( we don't really care about type )
		list( $type, $data ) = $MR_i->get_last_metric_values( NULL, NULL, $metricid );
		
		if( count( $data ) == 0 ) {
			// TODO issue warnings
			$lemon_lrf_error = "No values have been returned by lemon.";
			// return back to normal
			error_reporting( $own_logLevel );
			chdir( $own_wd);	
			return FALSE;
		}
		
		// data is an array of the that contains hosts as keys and metric values as values
		foreach( $data as $node => $value ) {
			// add to every host of this metric
			$metrics[ $metricKey ]['NODES'][] = array( 'id' => $node, 'name' => $node, 'value' => $value );
		} 
	}
	
	// return back to normal
	error_reporting( $own_logLevel );
	chdir( $own_wd);	
	
	// should be done
	return TRUE;
}

function lemon_lrf_getError() {
	global $lemon_lrf_error;
	return $lemon_lrf_error;
}

function lemon_lrf_requiresClientList() {
	return FALSE; // the lrs provides a method to get data for all clients of a metric
}

?>
