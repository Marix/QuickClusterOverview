<?php
/**
 * A data retrieval plugin for ganglia.
 *
 * @author Matthias Bach <marix (at) marix (dot) org>
 * @version 0.1
 * @since 0.1
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
/*
 * the methods implementing the interface can be found
 * all the way on the bottom of this file
 */
 
/*
 * Constants that influence the behaviour of the pluging
 */
/**
 * Constants defines the size of the data chunks read from
 * the socket when recieving the xml file.
 */
global $GANGLIA_CHUNK_SIZE;
$GANGLIA_CHUNK_SIZE = 8 * 1024;

/*
 * Variable that are internal to the plugin
 */ 
/**
 * Holds the human readable error string in case there was one.
 */
global $ganglia_error;
/**
 * Holds the list of all metrics for access by the xml parser callback.
 */
global $ganglia_metrics;

/**
 * Just a counter to be able to add the nodes as list
 */
global $ganglia_nodeIdx;
$ganglia_nodeIdx = -1;  // first node will increment and write to index 0
/**
 * Temporary store for the node IP
 */
global $ganglia_nodeIP;
/**
 * Temporary store for the node name
 */
global $ganglia_nodeName;

/*
 * private methods only used within the plugin
 */

/**
 * Callback function for xml-parser to handly xml-elements 
 */
function ganglia_start_element( $parser, $element, $attribs ) {
	global $ganglia_nodeIdx;
	global $ganglia_nodeIP;
	global $ganglia_nodeName;
	
	global $ganglia_metrics;
	
	global $ganglia_error;
	
	switch( $element ) {
		case 'GRID':
//			$grid = $attribs['NAME'];
			break;
		case 'CLUSTER':
//			$cluster = $attribs['NAME'];
			break;
		case 'HOST':
			$ganglia_nodeIP = $attribs['IP'];
			$ganglia_nodeName = $attribs['NAME'];
			$ganglia_nodeIdx++;
			break;
		case 'METRIC':
			// first check metric data
			$metricName = $attribs['NAME'];
			if( isset( $ganglia_metrics[ $metricName ] ) ) { // only supply if it is wanted. don't have to fill memory without need -> hurts serialisation
				$metric =& $ganglia_metrics[ $metricName ];
				$metric['name'] = $metricName;
				$metric['id'] = $metricName;
				// add current host + data to this metric
				$metric['NODES'][ $ganglia_nodeIdx ]  = array( 'id' => $ganglia_nodeIP, 'name' => $ganglia_nodeName, 'value' => $attribs['VAL'] );
			}
			break;
		case 'GANGLIA_XML';
			break;
		default:
			$ganglia_error =  "Unknown element: " . $element . "\n";
	}
}

/**
 * Dummy callback for element end
 */
function ganglia_end_element( $parser, $element ) {
}

/**
 * public methods that will be called during interface usage
 */
function ganglia_getName() {
	return 'Ganglia';
}

function ganglia_getDescription() {
	return 'A plugin that retrievs the system status either from a Gmetad or a Gmond.';
}

function ganglia_getVersion() {
	return 0.1;
}

function ganglia_getAuthor() {
	return 'Matthias Bach <marix (at) marix (dot) org>';
}

function ganglia_getInterfaceVersion() {
	return 0.1;
}

function ganglia_getData( &$metrics, $config ) {

	// import some global variables
	global $ganglia_error; // for error reporting
	global $ganglia_metrics;
	global $GANGLIA_CHUNK_SIZE; 
	
	// first extract important config data
	$sources = $config[ 'DATASOURCE' ][ 'PROVIDER' ];
	if( sizeof( $sources ) == 0 ) {
		$ganglia_error = 'No sources given';
		return FALSE;
	}
	
	// TODO loop over multiple sources if available
	// for now get first and be happy
	$host = $sources[0]['name'];
	$port = $sources[0]['port'];
	
	
	// make list of metrics available to xml callback handler
 	$ganglia_metrics = $metrics;
	
	// prepare parser for xml handling
	$xml_parser = xml_parser_create( );
	// register element handler
	xml_set_element_handler( $xml_parser, "ganglia_start_element", "ganglia_end_element" );

	
	// open the server to read the data from. TODO custom timeout and failover
	// we ignore warnings as we check for error anyways and it screws up the ui
	$fp = @fsockopen( $host, $port, $errno, $errstr, 30 );
	if( ! $fp ) {
		$ganglia_error = 'Failed to connect to host ' . $host . ':' . $port . ': ' . $errstr . "\n";
		return FALSE;
	}
	
	// read chunks of data feed them to the parser
	while( ($datachunk = fread( $fp, $GLOBALS['GANGLIA_CHUNK_SIZE'] )) && $ganglia_error == '' ) {
		if( ! xml_parse( $xml_parser, $datachunk, feof( $fp ) ) ) {
		
			$ganglia_error = 'Failed to parse the ganlia xml file: ' . xml_error_string( xml_get_error_code( $xml_parser ) ) . ' in line ' . xml_get_current_line_number( $xml_parser ) . " \n";
			fclose( $fp );
			return FALSE;
		}
	}
	
	if( $ganglia_error != '' ) {
		return FALSE;
	}
	
	// clean up after yourself
	fclose( $fp );

	// copy list of metrics back, actually $ganglia_metrics should be a ref to $metrics, but there seems to be a bug in php.
	$metrics = $ganglia_metrics;	
	
	
	// done and successfull
	return TRUE;	
}

function ganglia_getError( ) {
	global $ganglia_error;
	return $ganglia_error;
}

function ganglia_requiresClientList( ) {
	// ganglia always returns all clients
	return FALSE;
}

?>
