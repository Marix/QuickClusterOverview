<?php
/**
 * Main UI
 * 
 * This is the main page of the standalong ui. Here the graphs generated from the
 * cluster data will be shown and refreshs will be triggered if needed.
 * 
 * @author Matthias Bach
 * @since 0.1
 * @version 0.2
 * 
 * Copyright (C) 2006-2010  Matthias Bach
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
require_once( 'contrib/timer.php' );
$scriptTimer = new Timer();

// pull data from cluster/cache
$dataRetrievalTimer = new Timer();
require_once( 'inc/engine.php' );
$data = getData();
$dataRetrievalTimer->stop();

// prepare the data
require_once( 'contrib/smarty/Smarty.class.php' );
$smarty = new Smarty();

if( $data == NULL ) {
	$smarty->assign( 'error', getError() );
	$smarty->assign( 'stylesheet', resolveStylesheet() );
} else {
	$smarty->assign( 'stylesheet', resolveStylesheet( $data['CONFIG'] ) );
	$smarty->assign( 'updateIntervall', $data['CONFIG']['intervall'] );

	$graphs = array();
	foreach( $data['CONFIG']['GRAPHS'] as $GRAPH ) {
		$graphs[] = $data['CONFIG']['graphBasePath'] . $GRAPH['file'];
	}
	
	$smarty->assign( 'graphs', $graphs );
	
	
	// check whether the node list shall be shown
	$showNodeList = $data['CONFIG']['showNodeList'];
	// if none or show it cannot be overwritten
	if( $showNodeList != 'none' && $showNodeList != 'show' ) {
		// otherwise it might be requested via url
		if( $_GET['showNodeList'] == 'show' )
			$showNodeList = 'show';
		else // or not
			$showNodeList = 'optional';
	}
	$smarty->assign( 'showNodeList', $showNodeList );

	if( $showNodeList == 'show' ) {	
		// add list of nodes to results if needed (exemplary for first metric)
		$nodes = array();
		$currentSet = (int) $CLUSTER_DATA['currentSet'];
		foreach( $data['CLUSTER_DATA']['DATA_SETS'][ $currentSet ] as $key => $metric ) {
			if( $key == 'timestamp' )
				continue; // we don't want to loop over the timestamp ;)
				
			foreach( $metric['NODES'] as $NODE ) {
				if( isset( $NODE['NAME'] ) ) {
					$nodes[] = $NODE['NAME'];
				} else {
					$nodes[] = $NODE['id'];
				}
			}
			
			// other metrics shouldn't provider more info
			break;
		}
		$smarty->assign( 'nodes', $nodes );
	}

}


$smarty->assign( 'scriptTimer', $scriptTimer );
$smarty->assign( 'dataRetrievalTimer', $dataRetrievalTimer );


// render the data
$renderTimer = new Timer();
$smarty->assign( 'renderTimer', $renderTimer );
$smarty->display( 'index.tpl' );

// done
?>
